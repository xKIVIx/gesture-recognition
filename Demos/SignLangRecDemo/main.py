import cv2
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision
import matplotlib.pyplot as plt
import time
import os
import copy


from torchvision import datasets, models, transforms
from torch.optim import lr_scheduler
from torchsummary import summary
from torch.autograd import Variable
from PIL import Image

image_datasets = {}
dataloaders = {}
dataset_sizes = {}
class_names = []
device = 'cpu'
softmax = nn.Softmax()
transform_pipeline = transforms.Compose([transforms.Resize(256),
                                         transforms.CenterCrop(224),
                                         transforms.ToTensor(),
                                         transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                              std=[0.229, 0.224, 0.225])])

def cls():
    print('\n'*80)
    
def on_pain(frame):
    y = frame.shape[0]
    x = frame.shape[1]

    #cv2.rectangle(frame, (int(x / 2), 0), (x, y), (0, 255, 0), -1)
    #cv2.displayStatusBar('Video', text)
    cv2.imshow('Video', frame)
    
def init():
    global class_names
    class_names = [0,1,2,3,4,5,6,7,8,9]

def load_test_data():
    global image_datasets
    global dataloaders
    global dataset_sizes
    global class_names
    global device
    global transform_pipeline
    
    data_transforms = {
        'Test': transform_pipeline,
    }

    data_dir = './'
    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x), data_transforms[x]) for x in ['Test']}
    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=8, shuffle=True, num_workers=4) for x in ['Test']}
    dataset_sizes = {x: len(image_datasets[x]) for x in ['Test']}
    class_names = image_datasets['Test'].classes
    #print("cuda.is_available : " + str(torch.cuda.is_available()))


def test_model_on_testdata(model):
    print("Predicting test data...")
    running_corrects = 0
    for i, (inputs, labels) in enumerate(dataloaders['Test']):
        inputs = inputs.to(device)
        labels = labels.to(device)

        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        running_corrects += torch.sum(preds == labels.data)

    # print(running_loss)
    print(str(running_corrects.item()) + ' / ' + str(dataset_sizes['Test']))
    print(str(running_corrects.item() / dataset_sizes['Test']) + " %")

def load_model(path):
    model_ft = models.vgg16(pretrained=False)

    mod = list(model_ft.classifier.children())
    mod.pop()
    mod.append(nn.Linear(4096, 10))

    model_ft.classifier = nn.Sequential(*mod)
    #model_ft = model_ft.to(device)
    model_ft.load_state_dict(torch.load(path, map_location='cpu'))
    model_ft.eval()
    return model_ft

def show_model_vgg(model):
    summary(model, (3, 224, 224))

def transform_frame_to_np(frame):
    frame = cv2.resize(frame, (224, 224))
    #frame = np.array(frame).reshape((3,224,224))
    frame = Image.fromarray(frame)
    return frame

def show_predict_info(outputs, numberOfClasses):
    _, preds = torch.max(outputs, 1)
    outputs = softmax(outputs)
    cv2.displayStatusBar('Video', str(preds.item()))
    for i in range(numberOfClasses):
        cv2.setTrackbarPos(str(i),'Video',int((outputs[0][i].item())*100))

def nothing(num):
    pass

def create_trackBar(numberOfClasses):
    for i in range(numberOfClasses):
        cv2.createTrackbar(str(i), 'Video', 0, 100, nothing)

def main():
    #load_test_data()
    init()
    model = load_model('model_weight')
    show_model_vgg(model)
    #test_model_on_testdata(model)

    cap = cv2.VideoCapture(0)
    cv2.namedWindow('Video', cv2.WINDOW_NORMAL)
    create_trackBar(len(class_names))

    while (True):
        ret, frame = cap.read()
        tr_frame = transform_frame_to_np(frame)
        tr_frame = transform_pipeline(tr_frame)
        tr_frame = tr_frame.unsqueeze(0)
        tensor = Variable(tr_frame)
        outputs = model(tensor)
        show_predict_info(outputs,len(class_names))
        on_pain(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
    
    
main()
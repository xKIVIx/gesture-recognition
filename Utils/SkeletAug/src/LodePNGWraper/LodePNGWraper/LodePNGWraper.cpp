﻿#define DLL_EXPORTS
#include "LodePNGWraper.h"

#include <vector>

#include "lodepng.h"

namespace LodePNGWraper
{

    struct ResultData
    {
        std::vector<byte> data;
        unsigned width, height, bitdepth;
        PixelType pixelType;
    };

    LodePNGColorType GetPngType(PixelType pixelType, bool* isOk)
    {
        *isOk = true;
        switch (pixelType)
        {
        case LodePNGWraper::RGB:
            return LodePNGColorType::LCT_RGB;
        case LodePNGWraper::RGBA:
            return LodePNGColorType::LCT_RGBA;
        case LodePNGWraper::GRAY:
            return LodePNGColorType::LCT_GREY;
        case LodePNGWraper::GRAY_A:
            return LodePNGColorType::LCT_GREY_ALPHA;
        default:
            *isOk = false;
            return LodePNGColorType::LCT_GREY;
        }
    }

    ResultData * Encode(byte * data, unsigned width, unsigned height, PixelType pixelType, unsigned bitdepth, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data is nullptr\0");
            return nullptr;
        }

        bool isSuccessDetectType;

        lodepng::State state;
        state.encoder.filter_palette_zero = 0; //We try several filter types, including zero, allow trying them all on palette images too.
        state.encoder.add_id = false; //Don't add LodePNG version chunk to save more bytes
        state.encoder.text_compression = 1; //Not needed because we don't add text chunks, but this demonstrates another optimization setting
        state.encoder.zlibsettings.nicematch = 258; //Set this to the max possible, otherwise it can hurt compression
        state.encoder.zlibsettings.lazymatching = 1; //Definitely use lazy matching for better compression
        state.encoder.zlibsettings.windowsize = 32768; //Use maximum possible window size for best compression
        state.info_raw.bitdepth = bitdepth;
        state.info_raw.colortype = GetPngType(pixelType, &isSuccessDetectType);


        if (!isSuccessDetectType)
        {
            errorCallback("unknown pixel type\0");
            return nullptr;
        }

        ResultData* result = new ResultData();
        int errCode;

        errCode = lodepng::encode(result->data, data, width, height, state);

        if (errCode != 0)
        {
            delete result;
            std::string message = "lodepng return error: ";
            message += lodepng_error_text(errCode);
            errorCallback(message.c_str());
            return nullptr;
        }
        result->width = width;
        result->height = height;
        result->bitdepth = bitdepth;
        result->pixelType = pixelType;
        return result;
    }

    void GetData(ResultData * data, byte * buffer, unsigned size, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr || buffer == nullptr)
        {
            errorCallback("data or buffer nullpoiinters");
            return;
        }
        memcpy_s(buffer, size, data->data.data(), data->data.size());
    }

    unsigned GetDataSize(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data nullpoiinter");
            return 0;
        }
        return data->data.size();
    }

    unsigned GetHeight(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data nullpoiinter");
            return 0;
        }
        return data->height;
    }

    unsigned GetWidth(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data nullpoiinter");
            return 0;
        }
        return data->width;
    }

    ResultData * Decode(byte * data, unsigned dataSize, PixelType pixelType, unsigned bitdepth, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data is nullptr\0");
            return nullptr;
        }

        bool isSuccessDetectType;
        lodepng::State state;
        state.info_raw.bitdepth = bitdepth;
        state.info_raw.colortype = GetPngType(pixelType, &isSuccessDetectType);
        if (!isSuccessDetectType)
        {
            errorCallback("unknown pixel type\0");
            return nullptr;
        }

        ResultData* result = new ResultData();
        int errCode = lodepng::decode(result->data, result->width, result->height, state, data, dataSize);
        
        if (errCode != 0)
        {
            delete result;
            std::string message = "lodepng return error: ";
            message += std::to_string(errCode);
            errorCallback(message.c_str());
            return nullptr;
        }
        result->bitdepth = bitdepth;
        result->pixelType = pixelType;
        return result;
    }

    void Dispose(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data is nullptr\0");
            return;
        }

        delete data;

    }
}

#include "stdafx.h"
#include "CppUnitTest.h"

#include <JPEGTurbo.h>
#include <iostream>
#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#pragma comment(lib, "JPEGTurbo.lib")

namespace UnitTest
{
    void callBack(const char* message)
    {
        std::cout << message << std::endl;
        Assert::Fail();
    }

    TEST_CLASS(UnitTest1)
    {
    public:

        TEST_METHOD(TestRGB8)
        {
            JPEGTurbo::byte bytes[30000];

            for (unsigned i = 0; i < 30000; i++)
            {
                bytes[i] = i / (30000 / UCHAR_MAX);
            }

            JPEGTurbo::ResultData* h = JPEGTurbo::Encode(bytes, 100, 100, JPEGTurbo::RGB, 8, callBack);

            std::fstream file;
            file.open("testRGB8.jpg", std::ios::end);

            unsigned size = JPEGTurbo::GetDataSize(h, callBack);

            JPEGTurbo::byte* data = new JPEGTurbo::byte[size];
            JPEGTurbo::GetData(h, data, size, callBack);
            file.write((char*)data, size);
            file.close();
            JPEGTurbo::Dispose(h, callBack);
            delete[] data;
        }

        TEST_METHOD(TestG8)
        {
            JPEGTurbo::byte bytes[10000];

            for (unsigned i = 0; i < 10000; i++)
            {
                bytes[i] = i / (10000 / UCHAR_MAX);
            }

            JPEGTurbo::ResultData* h = JPEGTurbo::Encode(bytes, 100, 100, JPEGTurbo::GRAY, 8, callBack);

            std::fstream file;
            file.open("testG8.jpg", std::ios::end);

            unsigned size = JPEGTurbo::GetDataSize(h, callBack);

            JPEGTurbo::byte* data = new JPEGTurbo::byte[size];
            JPEGTurbo::GetData(h, data, size, callBack);
            file.write((char*)data, size);
            file.close();
            JPEGTurbo::Dispose(h, callBack);
            delete[] data;
        }

        TEST_METHOD(TestG16)
        {
            unsigned short bytes[10000];

            for (unsigned i = 0; i < 10000; i++)
            {
                bytes[i] = i;
            }

            JPEGTurbo::ResultData* h = JPEGTurbo::Encode((JPEGTurbo::byte*)bytes, 100, 100, JPEGTurbo::GRAY, 16, callBack);

            std::fstream file;
            file.open("testG16.jpg", std::ios::end);

            unsigned size = JPEGTurbo::GetDataSize(h, callBack);

            JPEGTurbo::byte* data = new JPEGTurbo::byte[size];
            JPEGTurbo::GetData(h, data, size, callBack);
            file.write((char*)data, size);
            file.close();
            JPEGTurbo::Dispose(h, callBack);
            delete[] data;
        }
    };
}
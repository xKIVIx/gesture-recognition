#pragma once

#ifdef DLL_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif

extern "C"
{
    namespace JPEGTurbo
    {
        typedef unsigned char byte;

        enum PixelType
        {
            RGB = 0,
            RGBA = 1,
            GRAY = 2,
            GRAY_A = 3
        };

        struct ResultData;

        typedef void(*ERROR_CALLBACK)(const char * message);

        DLL_API ResultData* Encode(byte* data,
            unsigned width,
            unsigned height,
            PixelType pixelType,
            unsigned bitdepth,
            ERROR_CALLBACK errorCallback);

        DLL_API void GetData(ResultData* data,
            byte* buffer,
            unsigned size,
            ERROR_CALLBACK errorCallback);

        DLL_API unsigned GetDataSize(ResultData* data, ERROR_CALLBACK errorCallback);

        DLL_API unsigned GetHeight(ResultData* data, ERROR_CALLBACK errorCallback);

        DLL_API unsigned GetWidth(ResultData* data, ERROR_CALLBACK errorCallback);

        DLL_API ResultData* Decode(byte* data,
            unsigned dataSize,
            PixelType pixelType,
            unsigned bitdepth,
            ERROR_CALLBACK errorCallback);

        DLL_API void Dispose(ResultData* data, ERROR_CALLBACK errorCallback);
    }
}
﻿#define DLL_EXPORTS

#include "JPEGTurbo.h"
#include <cstdlib>
#include <turbojpeg.h>
#include <string>

#pragma comment(lib, "jpeg.lib")
#pragma comment(lib, "turbojpeg.lib")

using namespace JPEGTurbo;

typedef unsigned short uint16;

namespace JPEGTurbo
{
    const uint16 CONVERT_COEF_16_TO_8 = USHRT_MAX / UCHAR_MAX;

    struct ResultData
    {
        byte* data;
        unsigned width, height, bitdepth;
        PixelType pixelType;
        unsigned long dataSize;
    };

    TJPF ConvertPixelFormat(PixelType pixelType)
    {
        switch (pixelType)
        {
        case JPEGTurbo::RGB:
            return TJPF_RGB;
        case JPEGTurbo::RGBA:
            return TJPF_RGBA;
        case JPEGTurbo::GRAY:
            return TJPF_GRAY;
        default:
            return TJPF_UNKNOWN;
        }
    }

    DLL_API byte* Convert16To8(uint16* data, unsigned size)
    {
        byte* result = (byte*)malloc(size);
        for (unsigned i = 0; i < size; i++)
        {
            result[i] = data[i] / CONVERT_COEF_16_TO_8;
        }
        return result;
    }

    DLL_API ResultData * Encode(byte * data, unsigned width, unsigned height, PixelType pixelType, unsigned bitdepth, ERROR_CALLBACK errorCallback)
    {
        bool isCoverted = false;
        if (bitdepth == 16)
        {
            data = Convert16To8((uint16*)data, width * height);
            isCoverted;
        }
        else if (bitdepth != 8)
        {
            errorCallback("Unsupport bitdepth");
            return nullptr;
        }

        TJPF tjPixelFormat = ConvertPixelFormat(pixelType);
        if (tjPixelFormat == TJPF_UNKNOWN)
        {
            errorCallback("Unsupport pixel format");
            return nullptr;
        }

        TJSAMP samp = TJSAMP_444;
        if (tjPixelFormat == TJPF_GRAY)
        {
            samp = TJSAMP_GRAY;
        }

        tjhandle handle = tjInitCompress();

        if (handle == NULL)
        {
            errorCallback(tjGetErrorStr2(handle));
            return nullptr;
        }
        ResultData* result = new ResultData;

        result->dataSize = tjBufSize(width, height, TJSAMP_444);
        result->data = (byte*)malloc(result->dataSize);
        result->width = width;
        result->height = height;
        result->bitdepth = bitdepth;
        result->pixelType = pixelType;
        int errCode = tjCompress2(handle,
            data,
            width,
            0,
            height,
            tjPixelFormat,
            &result->data,
            &result->dataSize,
            samp,
            100,
            TJFLAG_FASTDCT | TJFLAG_ACCURATEDCT | TJFLAG_PROGRESSIVE);

        if (errCode == -1)
        {
            errorCallback(tjGetErrorStr2(handle));
            tjDestroy(handle);
            delete result;
            return nullptr;
        }
        tjDestroy(handle);
        if (isCoverted)
        {
            free(data);
        }
        return result;
    }

    void GetData(ResultData * data, byte * buffer, unsigned size, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr || buffer == nullptr)
        {
            errorCallback("data or buffer nullpoiinters");
            return;
        }
        memcpy_s(buffer, size, data->data, data->dataSize);
    }

    unsigned GetDataSize(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data nullpoiinter");
            return 0;
        }
        return data->dataSize;
    }

    unsigned GetHeight(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data nullpoiinter");
            return 0;
        }
        return data->height;
    }

    unsigned GetWidth(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data nullpoiinter");
            return 0;
        }
        return data->width;
    }

    ResultData * Decode(byte * data, unsigned dataSize, PixelType pixelType, unsigned bitdepth, ERROR_CALLBACK errorCallback)
    {
        errorCallback("Not release");
        return nullptr;
    }

    void Dispose(ResultData * data, ERROR_CALLBACK errorCallback)
    {
        if (data == nullptr)
        {
            errorCallback("data is nullptr\0");
            return;
        }
        free(data->data);
        delete data;
    }
}
/******************************************************************************
 * Copyright (C) Leap Motion, Inc. 2011-2018.                                 *
 * Leap Motion proprietary and confidential.                                  *
 *                                                                            *
 * Use subject to the terms of the Leap Motion SDK Agreement available at     *
 * https://developer.leapmotion.com/sdk_agreement, or another agreement       *
 * between Leap Motion and you, your company or other organization.           *
 ******************************************************************************/
#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Leap.Unity
{
    [CustomEditor(typeof(LeapImageRetriever))]
    public class LeapImageRetrieverEditor: CustomEditorBase<LeapImageRetriever>
    {
        #region Private Fields

        private GUIContent _distortionTextureGUIContent;
        private GUIContent _textureGUIContent;

        #endregion Private Fields

        #region Public Methods

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (Application.isPlaying)
            {
                var data = target.TextureData;
                var dataType = typeof(Object);

                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField(_textureGUIContent, data.TextureData.CombinedTexture, dataType, true);
                EditorGUILayout.ObjectField(_distortionTextureGUIContent, data.Distortion.CombinedTexture, dataType, true);
                EditorGUI.EndDisabledGroup();
            }
        }

        #endregion Public Methods

        #region Protected Methods

        protected override void OnEnable()
        {
            base.OnEnable();

            _textureGUIContent = new GUIContent("Sensor Texture");
            _distortionTextureGUIContent = new GUIContent("Distortion Texture");
        }

        #endregion Protected Methods
    }
}

#endif //UNITY_EDITOR
/******************************************************************************
 * Copyright (C) Leap Motion, Inc. 2011-2018.                                 *
 * Leap Motion proprietary and confidential.                                  *
 *                                                                            *
 * Use subject to the terms of the Leap Motion SDK Agreement available at     *
 * https://developer.leapmotion.com/sdk_agreement, or another agreement       *
 * between Leap Motion and you, your company or other organization.           *
 ******************************************************************************/
#if UNITY_EDITOR

using UnityEditor;

namespace Leap.Unity
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(XRHeightOffset))]
    public class XRHeightOffsetEditor: CustomEditorBase<XRHeightOffset>
    {
        #region Protected Methods

        protected override void OnEnable()
        {
            base.OnEnable();

            specifyConditionalDrawing(conditionalName: "recenterOnKey",
                                      dependantProperties: "recenterKey");
        }

        #endregion Protected Methods
    }
}

#endif
﻿using System;
using System.Runtime.InteropServices;

using UnityEngine;

public static class LodePNG
{
    #region Public Enums

    public enum PixelType
    {
        RGB = 0,
        RGBA = 1,
        GRAY = 2,
        GRAY_A = 3
    };

    #endregion Public Enums

    #region Public Structs

    public struct Image
    {
        #region Public Fields

        public uint bitdepth;
        public byte[] pixelsData;
        public PixelType pixelType;
        public uint width, height;

        #endregion Public Fields
    }

    #endregion Public Structs

    #region Private Fields

    private static ErrorCallback _errorCallback = (message) =>
    {
        Debug.LogError(message);
    };

    #endregion Private Fields

    #region Public Delegates

    public delegate void ErrorCallback(string message);

    #endregion Public Delegates

    #region Public Methods

    /// <summary>
    /// Декодирование png данных.
    /// </summary>
    /// <param name="data">Декодируемые данных</param>
    /// <param name="pixelType">Тип пикселя</param>
    /// <param name="bitdepth">Глубина цвета</param>
    /// <returns>Декодированные данные</returns>
    public static Image Decode(byte[] data, PixelType pixelType, uint bitdepth)
    {
        IntPtr ptr = Decode(data, (uint)data.Length, pixelType, bitdepth, _errorCallback);
        if (ptr == null)
        {
            Debug.LogError("Result is null");
            return new Image();
        }
        var image = new Image()
        {
            width = GetWidth(ptr, _errorCallback),
            height = GetHeight(ptr, _errorCallback),
            pixelType = pixelType,
            bitdepth = bitdepth
        };
        var dataSize = GetDataSize(ptr, _errorCallback);
        image.pixelsData = new byte[dataSize];
        GetData(ptr, image.pixelsData, dataSize, _errorCallback);
        return image;
    }

    /// <summary>
    /// Создание данных png файла.
    /// </summary>
    /// <param name="data">Данные для конвертации</param>
    /// <param name="width">Ширина картинки</param>
    /// <param name="height">Высота картинки</param>
    /// <param name="pixelType">Формат пикселя</param>
    /// <param name="bitdepth">Глубина цвета</param>
    /// <returns>Бинарное содержание png</returns>
    public static byte[] Encode(byte[] data, uint width, uint height, PixelType pixelType, uint bitdepth)
    {
        var ptr = Encode(data, width, height, pixelType, bitdepth, _errorCallback);
        if (ptr == null)
        {
            Debug.LogError("Result is null");
            return null;
        }

        uint dataSize = GetDataSize(ptr, _errorCallback);
        var resultData = new byte[dataSize];
        GetData(ptr, resultData, dataSize, _errorCallback);
        Dispose(ptr, _errorCallback);
        return resultData;
    }

    #endregion Public Methods

    #region Private Methods

    [DllImport("LodePNGWraper")]
    private static extern IntPtr Decode(byte[] data,
                                        uint dataSize,
                                        PixelType pixelType,
                                        uint bitdepth,
                                        ErrorCallback errorCallback);

    [DllImport("LodePNGWraper")]
    private static extern void Dispose(IntPtr data, ErrorCallback errorCallback);

    [DllImport("LodePNGWraper")]
    private static extern IntPtr Encode(byte[] data,
                                        uint width,
                                        uint height,
                                        PixelType pixelType,
                                        uint bitdepth,
                                        ErrorCallback errorCallback);

    [DllImport("LodePNGWraper")]
    private static extern void GetData(IntPtr data,
                                       byte[] buffer,
                                       uint size,
                                       ErrorCallback errorCallback);

    [DllImport("LodePNGWraper")]
    private static extern uint GetDataSize(IntPtr data, ErrorCallback errorCallback);

    [DllImport("LodePNGWraper")]
    private static extern uint GetHeight(IntPtr data, ErrorCallback errorCallback);

    [DllImport("LodePNGWraper")]
    private static extern uint GetWidth(IntPtr data, ErrorCallback errorCallback);

    #endregion Private Methods
}
﻿using NUnit.Framework;

using Oranges.Utils;

using UnityEngine;

public class MathTests
{
    #region Public Methods

    // A Test behaves as an ordinary method
    [Test]
    public void MathTestsSimplePasses()
    {
        var points = new Vector3[]
        {
                new Vector3{ x = 3, y = 11, z = 21},
                new Vector3{ x = 5, y = 11, z = 21},
                new Vector3{ x = 4, y = 10, z = 21},
                new Vector3{ x = 4, y = 12, z = 21},
                new Vector3{ x = 4, y = 11, z = 22},
                new Vector3{ x = 4, y = 11, z = 20}
        };

        var pointsResult = new Vector3[]
        {
                new Vector3{ x = -1, y = 0, z = 0},
                new Vector3{ x = 1, y = 0, z = 0},
                new Vector3{ x = 0, y = -1, z = 0},
                new Vector3{ x = 0, y = 1, z = 0},
                new Vector3{ x = 0, y = 0, z = 1},
                new Vector3{ x = 0, y = 0, z = -1}
        };

        var centeredVector = Math.CenteredVector(points);
        for (var i = 0; i < points.Length; i++)
        {
            points[i] += centeredVector;
            Assert.AreEqual(pointsResult[i], points[i], $"Centered vector: {centeredVector.ToString()}");
        }
    }

    #endregion Public Methods
}
﻿using UnityEngine;

namespace Oranges.Utils
{
    public static class Math
    {
        #region Public Methods
        /// <summary>
        /// Вычисление центра кадра
        /// </summary>
        /// <param name="points">
        /// Координаты костей
        /// </param>
        /// <returns></returns>
        public static Vector3 CenteredVector(Vector3[] points)
        {
            Vector3 pointMin = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue),
                    pointMax = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            foreach (var point in points)
            {
                // x
                if (point.x < pointMin.x)
                {
                    pointMin.x = point.x;
                }
                else
                {
                    if (point.x > pointMax.x)
                    {
                        pointMax.x = point.x;
                    }
                }

                // y
                if (point.y < pointMin.y)
                {
                    pointMin.y = point.y;
                }
                else
                {
                    if (point.y > pointMax.y)
                    {
                        pointMax.y = point.y;
                    }
                }

                // z
                if (point.z < pointMin.z)
                {
                    pointMin.z = point.z;
                }
                else
                {
                    if (point.z > pointMax.z)
                    {
                        pointMax.z = point.z;
                    }
                }
            }

            var d = pointMax - pointMin;

            d /= 2.0f;

            return -(pointMin + d);
        }

        /// <summary>
        /// Получить рандомный вектор со значаниями между минимальным и максимальным векторами.
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <returns></returns>
        public static Vector3 GetRandomVector(Vector3 max, Vector3 min, int seed)
        {
            var randomizer = new System.Random(seed);

            var delta = max - min;

            var randomVector = new Vector3
            {
                x = (float)(randomizer.NextDouble() - 0.5) * delta.x,
                y = (float)(randomizer.NextDouble() - 0.5) * delta.y,
                z = (float)(randomizer.NextDouble() - 0.5) * delta.z,
            };

            return delta / 2 + randomVector;
        }

        /// <summary>
        /// Вычисление кординаты точки с добавлением шума
        /// </summary>
        /// <param name="x"></param>
        /// <param name="lastBone"></param>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        public static Vector3 GetSkeletonRandomVector(Vector3 x,
                                                      Vector3 lastBone,
                                                      Vector3 max,
                                                      Vector3 min,
                                                      int seed)
        {
            var centeredPoint = x - lastBone;
            var randomAngles = GetRandomVector(max, min, seed);
            var quaternion = Quaternion.Euler(randomAngles);
            return quaternion * centeredPoint + lastBone;
        }

        /// <summary>
        /// Вращение точки
        /// </summary>
        /// <param name="x"></param>
        /// <param name="lastBone"></param>
        /// <param name="anglesEuler"></param>
        /// <returns></returns>
        public static Vector3 GetSkeletonRandomVector(Vector3 x,
                                                      Vector3 lastBone,
                                                      Vector3 anglesEuler)
        {
            var centeredPoint = x - lastBone;
            var quaternion = Quaternion.Euler(anglesEuler);
            return quaternion * centeredPoint + lastBone;
        }

        #endregion Public Methods
    }
}
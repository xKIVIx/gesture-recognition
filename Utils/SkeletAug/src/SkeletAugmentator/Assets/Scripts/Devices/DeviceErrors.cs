﻿namespace Oranges.Devices
{
    public enum DeviceErrors
    {
        SUCCESSED,
        NOT_FOUND_DEVICE,
        RECORD_IS_STARTED,
        RECORD_NOT_STARTED,
        CAM_NOT_SUPPORT
    }
}
using Leap.Unity;

using UnityEngine;

using Zenject;

namespace Oranges.Devices
{
    internal class DevicesInstaller : MonoInstaller
    {
        #region Private Fields
#pragma warning disable CS0649
        [SerializeField] private LeapServiceProvider _leapService;
#pragma warning restore CS0649

        #endregion Private Fields

        #region Public Methods

        public override void InstallBindings()
        {
            IDevice[] devices = { new Kinect(),
                                  new LeapMotion(_leapService) };
            Container.BindInstance(devices);
        }

        #endregion Public Methods
    }
}
﻿using Oranges.Data;
using System;
using UniRx;
using UnityEngine;
using Windows.Kinect;

namespace Oranges.Devices
{
    internal class Kinect : IDevice
    {
        #region Public Constructors

        public Kinect()
        {
            _kinectSensor = KinectSensor.GetDefault();
            if (_kinectSensor != null)
            {
                Info.Value = new DeviceInfo()
                {
                    title = "Kinect",
                    isConnected = _kinectSensor.IsAvailable,
                    isStarted = _kinectSensor.IsOpen,
                    outputTextures = new Texture2D[0]
                };
            }
            else
            {
                Debug.LogError("Kinect don`t found");
            }

            Observable.Timer(System.TimeSpan.FromSeconds(3))
                      .RepeatSafe()
                      .Subscribe(x =>
                      {
                          if (_kinectSensor.IsAvailable != Info.Value.isConnected)
                          {
                              var devInfo = Info.Value;
                              devInfo.isConnected = _kinectSensor.IsAvailable;
                              devInfo.title = $"Kinect";
                              if (_kinectSensor.IsAvailable == false)
                              {
                                  Stop();
                              }
                              Info.Value = devInfo;
                          }
                      });
        }

        #endregion Public Constructors

        #region Private Destructors

        ~Kinect()
        {
            Stop();
        }

        #endregion Private Destructors

        #region Private Fields

        private BodyFrameReader _bodyFrameReader;
        private byte[] _bodyIndexFrameData;
        private BodyIndexFrameReader _bodyIndexFrameReader;
        private byte[] _colorFrameData;
        private ColorFrameReader _colorFrameReader;
        private ushort[] _depthFrameData;
        private DepthFrameReader _depthFrameReader;
        private InfraredFrameReader _infraredFrameReader;
        private ushort[] _irFrameData;

        /// <summary>
        /// Подключенный сенсор Kinect.
        /// </summary>
        private KinectSensor _kinectSensor;

        #endregion Private Fields

        #region Public Properties

        public ReactiveProperty<DeviceInfo> Info { get; private set; } = new ReactiveProperty<DeviceInfo>();

        #endregion Public Properties

        #region Public Methods

        public DeviceErrors GetRawLastFrames(out ImageData[] imagesData)
        {
            imagesData = new ImageData[Info.Value.outputTextures.Length];
            var deviceInfo = Info.Value;

            for (int i = 0; i < imagesData.Length; i++)
            {
                var texture = deviceInfo.outputTextures[i];
                imagesData[i] = new ImageData
                {
                    format = texture.format,
                    width = texture.width,
                    height = texture.height
                };
            }

            imagesData[0].bytes = (byte[])_colorFrameData.Clone();

            imagesData[1].bytes = new byte[_irFrameData.Length * sizeof(ushort)];
            Buffer.BlockCopy(_irFrameData, 0, imagesData[1].bytes, 0, imagesData[1].bytes.Length);

            imagesData[2].bytes = new byte[_depthFrameData.Length * sizeof(ushort)];
            Buffer.BlockCopy(_depthFrameData, 0, imagesData[2].bytes, 0, imagesData[2].bytes.Length);

            return DeviceErrors.SUCCESSED;
        }

        // Start is called before the first frame update
        public DeviceErrors Start()
        {
            var deviceInfo = Info.Value;

            if (deviceInfo.isStarted)
            {
                return DeviceErrors.RECORD_IS_STARTED;
            }

            deviceInfo.outputTextures = new Texture2D[3];

            var frameDesc = _kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Rgba);
            var newTexture = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.RGBA32, false);
            deviceInfo.outputTextures[0] = newTexture;
            _colorFrameData = new byte[frameDesc.BytesPerPixel * frameDesc.LengthInPixels];
            _colorFrameReader = _kinectSensor.ColorFrameSource.OpenReader();

            frameDesc = _kinectSensor.InfraredFrameSource.FrameDescription;
            deviceInfo.outputTextures[1] = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.R16, false);
            _irFrameData = new ushort[frameDesc.LengthInPixels];
            _infraredFrameReader = _kinectSensor.InfraredFrameSource.OpenReader();

            frameDesc = _kinectSensor.DepthFrameSource.FrameDescription;
            deviceInfo.outputTextures[2] = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.R16, false);
            _depthFrameData = new ushort[frameDesc.LengthInPixels];
            _depthFrameReader = _kinectSensor.DepthFrameSource.OpenReader();

            /*
            frameDesc = _kinectSensor.BodyIndexFrameSource.FrameDescription;
            deviceInfo.outputTextures[3] = new Texture2D(frameDesc.Width, frameDesc.Height, TextureFormat.R8, false);
            _bodyIndexFrameData = new byte[frameDesc.LengthInPixels];
            _bodyIndexFrameReader = _kinectSensor.BodyIndexFrameSource.OpenReader();
            */

            //_bodyFrameReader = _kinectSensor.BodyFrameSource.OpenReader();

            deviceInfo.isStarted = true;
            _kinectSensor.Open();

            Info.Value = deviceInfo;

            return _kinectSensor.IsOpen ? DeviceErrors.SUCCESSED : DeviceErrors.RECORD_NOT_STARTED;
        }

        public void Stop()
        {
            var deviceInfo = Info.Value;
            foreach (var texture in deviceInfo.outputTextures)
            {
                Texture2D.Destroy(texture);
            }
            deviceInfo.outputTextures = new Texture2D[0];
            deviceInfo.isStarted = false;
            Info.Value = deviceInfo;

            _colorFrameReader?.Dispose();
            _colorFrameReader = null;

            _infraredFrameReader?.Dispose();
            _infraredFrameReader = null;

            _depthFrameReader?.Dispose();
            _depthFrameReader = null;

            _bodyIndexFrameReader?.Dispose();
            _bodyIndexFrameReader = null;

            _bodyFrameReader?.Dispose();
            _bodyFrameReader = null;

            _kinectSensor?.Close();
        }

        public DeviceErrors СaptureFrame()
        {
            if (!Info.Value.isStarted)
            {
                return DeviceErrors.RECORD_NOT_STARTED;
            }

            /// Получение цветного кадра
            var colorFrame = _colorFrameReader.AcquireLatestFrame();
            if (colorFrame != null)
            {
                colorFrame.CopyConvertedFrameDataToArray(_colorFrameData, ColorImageFormat.Rgba);
                colorFrame.Dispose();
                colorFrame = null;
                var colorTexture = Info.Value.outputTextures[0];
                colorTexture.LoadRawTextureData(_colorFrameData);
                colorTexture.Apply();
            }

            /// Получение инфракрасного кадра.
            var irFrame = _infraredFrameReader.AcquireLatestFrame();
            if (irFrame != null)
            {
                irFrame.CopyFrameDataToArray(_irFrameData);
                irFrame.Dispose();
                irFrame = null;
                byte[] bytes = new byte[_irFrameData.Length * sizeof(ushort)];
                Buffer.BlockCopy(_irFrameData, 0, bytes, 0, bytes.Length);
                var irTexture = Info.Value.outputTextures[1];
                irTexture.LoadRawTextureData(bytes);
                irTexture.Apply();
            }

            /// Получение кадра с картой глубины
            var depthFrame = _depthFrameReader.AcquireLatestFrame();
            if (depthFrame != null)
            {
                depthFrame.CopyFrameDataToArray(_depthFrameData);
                depthFrame.Dispose();
                depthFrame = null;
                byte[] bytes = new byte[_depthFrameData.Length * sizeof(ushort)];
                Buffer.BlockCopy(_depthFrameData, 0, bytes, 0, bytes.Length);
                var depthTexture = Info.Value.outputTextures[2];
                depthTexture.LoadRawTextureData(bytes);
                depthTexture.Apply();
            }

            /*
            var bodyFrame = _bodyFrameReader.AcquireLatestFrame();
            if (bodyFrame != null)
            {
            }
            */

            /*
             * Неработает
            /// Получение кадра с выделение тела.
            var bodyIndexFrame = _bodyIndexFrameReader.AcquireLatestFrame();
            if (bodyIndexFrame != null)
            {
                bodyIndexFrame.CopyFrameDataToArray(_bodyIndexFrameData);
                bodyIndexFrame.Dispose();
                bodyIndexFrame = null;
                var depthTexture = Info.Value.outputTextures[3];
                depthTexture.LoadRawTextureData(_bodyIndexFrameData);
                depthTexture.Apply();
            }*/

            return DeviceErrors.SUCCESSED;
        }

        #endregion Public Methods
    }
}
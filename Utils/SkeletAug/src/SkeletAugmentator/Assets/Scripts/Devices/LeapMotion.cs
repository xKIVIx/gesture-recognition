﻿using Leap;
using Leap.Unity;

using Oranges.Data;
using System;
using UniRx;

using UnityEngine;

namespace Oranges.Devices
{
    internal class LeapMotion : IDevice
    {
        #region Public Constructors

        public LeapMotion(LeapServiceProvider leapServiceProvider)
        {
            var deviceInfo = new DeviceInfo()
            {
                title = "LeapMotion",
                isConnected = false,
                isStarted = false,
                outputTextures = new Texture2D[0],
                skelets = null
            };
            Info.Value = deviceInfo;

            Observable.Timer(System.TimeSpan.FromSeconds(1))
                      .RepeatSafe()
                      .Subscribe(x =>
                      {
                          if (_controller == null)
                          {
                              _controller = leapServiceProvider.GetLeapController();
                              if (_controller != null)
                              {
                                  _controller.ImageReady += HandleImage;
                                  _controller.FrameReady += HandleFrame;
                              }
                              else
                              {
                                  return;
                              }
                          }
                          if (_controller?.IsConnected != Info.Value.isConnected)
                          {
                              var devInfo = Info.Value;
                              devInfo.isConnected = _controller.IsConnected;
                              if (_controller.IsConnected == false)
                              {
                                  Stop();
                              }
                              Info.Value = devInfo;
                          }
                      });
        }

        #endregion Public Constructors

        #region Private Destructors

        ~LeapMotion()
        {
            _controller.ImageReady -= HandleImage;
            _controller.FrameReady -= HandleFrame;
            _controller.Dispose();
        }

        #endregion Private Destructors

        #region Private Fields

        private Controller _controller;
        private HandData _currentSkelets;
        private byte[] _outputTextureLeft, _outputTextureRight;

        private int _widthFrame, _heightFrame;

        #endregion Private Fields

        #region Public Properties

        public ReactiveProperty<DeviceInfo> Info { get; } = new ReactiveProperty<DeviceInfo>();

        #endregion Public Properties

        #region Public Methods

        public DeviceErrors GetRawLastFrames(out ImageData[] imagesData)
        {
            imagesData = new ImageData[2];
            imagesData[0] = new ImageData
            {
                format = TextureFormat.R8,
                width = _widthFrame,
                height = _heightFrame,
                bytes = (byte[])_outputTextureLeft.Clone()
            };

            imagesData[1] = new ImageData
            {
                format = TextureFormat.R8,
                width = _widthFrame,
                height = _heightFrame,
                bytes = (byte[])_outputTextureRight.Clone()
            };

            return DeviceErrors.SUCCESSED;
        }

        public DeviceErrors Start()
        {
            if (!Info.Value.isConnected)
            {
                Debug.LogError("Leap is not connection");
                return DeviceErrors.NOT_FOUND_DEVICE;
            }
            if (Info.Value.isStarted)
            {
                Debug.LogError("Leap is started");
                return DeviceErrors.RECORD_IS_STARTED;
            }
            if (_outputTextureLeft == null || _outputTextureRight == null)
            {
                Debug.LogError("Leap is not init");
                return DeviceErrors.NOT_FOUND_DEVICE;
            }
            var deviceInfo = Info.Value;
            deviceInfo.isStarted = true;
            deviceInfo.isConnected = true;
            deviceInfo.skelets = new ReactiveProperty<HandData>(new HandDataLeap() {
                Bones = new Vector3[HandDataLeap.COUNT_BONES * 2]
            });
            deviceInfo.outputTextures = new Texture2D[] { new Texture2D(_widthFrame,
                                                                        _heightFrame,
                                                                        TextureFormat.R8,
                                                                        false),
                                                          new Texture2D(_widthFrame,
                                                                        _heightFrame,
                                                                        TextureFormat.R8,
                                                                        false)};
            Info.Value = deviceInfo;
            return DeviceErrors.SUCCESSED;
        }

        public void Stop()
        {
            var deviceInfo = Info.Value;
            if (!Info.Value.isConnected)
            {
                deviceInfo.isConnected = false;
            }
            if (!Info.Value.isStarted)
            {
                return;
            }
            foreach (var texture in deviceInfo.outputTextures)
            {
                Texture2D.Destroy(texture);
            }
            deviceInfo.isStarted = false;
            deviceInfo.skelets = null;
            deviceInfo.outputTextures = new Texture2D[0];
            Info.Value = deviceInfo;
        }

        public DeviceErrors СaptureFrame()
        {
            var deviceInfo = Info.Value;
            if (!deviceInfo.isStarted)
            {
                return DeviceErrors.RECORD_NOT_STARTED;
            }
            var texture = deviceInfo.outputTextures[0];
            texture.LoadRawTextureData(_outputTextureLeft);
            texture.Apply();

            texture = deviceInfo.outputTextures[1];
            texture.LoadRawTextureData(_outputTextureRight);
            texture.Apply();

            deviceInfo.skelets.Value =_currentSkelets;

            return DeviceErrors.SUCCESSED;
        }

        #endregion Public Methods

        #region Private Methods

        private Vector3[] ConvertHand(Hand hand)
        {
            var result = new Vector3[HandDataLeap.COUNT_BONES];
            var countFingBones = 4;
            var countFings = 5;

            for (int i = 0; i < countFings; i++)
            {
                var fing = hand.Fingers[i];
                for (int boneId = 0; boneId < countFingBones; boneId++)
                {
                    result[countFingBones * i + boneId] = fing.bones[boneId].Center.ToVector3();
                }
            }

            return result;
        }

        /// <summary>
        /// Обработчик распознаных рук.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private void HandleFrame(object obj, FrameEventArgs args)
        {
            var frame = args.frame;
            

            var bones = new Vector3[HandDataLeap.COUNT_BONES * 2];

            if (frame.Hands.Count == 1)
            {
                var hand = frame.Hands[0];
                if (hand.IsLeft)
                {
                    Array.Copy(ConvertHand(hand), bones, HandDataLeap.COUNT_BONES);
                }
                else
                {
                    Array.Copy(ConvertHand(hand), 0, bones, HandDataLeap.COUNT_BONES, HandDataLeap.COUNT_BONES);
                }
            }
            else if (frame.Hands.Count == 2)
            {
                var hand = frame.Hands[0];
                if (hand.IsLeft)
                {
                    Array.Copy(ConvertHand(hand), bones, HandDataLeap.COUNT_BONES);
                    Array.Copy(ConvertHand(frame.Hands[1]), 0, bones, HandDataLeap.COUNT_BONES, HandDataLeap.COUNT_BONES);
                }
                else
                {
                    Array.Copy(ConvertHand(hand), 0, bones, HandDataLeap.COUNT_BONES, HandDataLeap.COUNT_BONES);
                    Array.Copy(ConvertHand(frame.Hands[1]), bones, HandDataLeap.COUNT_BONES);
                }
            }

            _currentSkelets = new HandDataLeap()
            {
                Bones = bones
            };
        }

        /// <summary>
        /// Обработчик получения нового изображения с камер.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private void HandleImage(object obj, ImageEventArgs args)
        {
            Image image = args.image;
            if (_outputTextureLeft == null)
            {
                _widthFrame = image.Width;
                _heightFrame = image.Height;
            }
            _outputTextureLeft = image.Data(Image.CameraType.LEFT);
            _outputTextureRight = image.Data(Image.CameraType.RIGHT);
        }

        #endregion Private Methods
    }
}
﻿using Oranges.Data;

using UniRx;

namespace Oranges.Devices
{
    public interface IDevice
    {
        #region Public Properties

        ReactiveProperty<DeviceInfo> Info { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Получить byte данные последнего фрейма.
        /// Бытрее чем из Texture2d
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        DeviceErrors GetRawLastFrames(out ImageData[] imagesData);

        /// <summary>
        /// Запустить запись.
        /// </summary>
        /// <returns></returns>
        DeviceErrors Start();

        /// <summary>
        /// Остановить запись.
        /// </summary>
        void Stop();

        /// <summary>
        /// Захват текущего фрейма.
        /// </summary>
        /// <returns></returns>
        DeviceErrors СaptureFrame();

        #endregion Public Methods
    }
}
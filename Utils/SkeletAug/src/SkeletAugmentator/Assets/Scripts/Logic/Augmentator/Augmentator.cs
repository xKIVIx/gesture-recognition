﻿using Oranges.Data;
using Oranges.Utils;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Oranges.Logic.Augmentator
{
    /// <summary>
    /// Класс выполняющий аугментацию данных
    /// </summary>
    internal class Augmentator : IAugmentator
    {
        #region Public Properties

        /// <summary>
        /// <see cref="IAugmentator.Settings"/>
        /// </summary>
        public AugmentatorSettings Settings { get; set; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// <see cref="IAugmentator.Augment(HandData[])"/>
        /// </summary>
        public HandData[] Augment(HandData[] gesture) => Augment(gesture, Settings.seed);

        /// <summary>
        /// <see cref="IAugmentator.Augment(HandData[], int)"/>
        /// </summary>
        public HandData[] Augment(HandData[] gesture, int seed)
        {
            var randomizer = new System.Random(seed);
            ParallelQuery<HandData> result = gesture.AsParallel()
                                                    .AsOrdered()
                                                    .Select(x => (HandData)x.Clone());

            if (Settings.minNoise != null && Settings.maxNoise != null)
            {
                result = result.Select(x => Noise(x, seed));
            }

            var bones = result.Select(x => x.Bones);

            if (Settings.maxShift != null && Settings.minShift != null)
            {
                var shiftVector = Math.GetRandomVector(Settings.maxShift,
                                                       Settings.minShift,
                                                       randomizer.Next());

                bones = bones.Select(x => Shift(x, shiftVector));
            }

            if (Settings.isCenetered)
            {
                var centeredVector = Math.CenteredVector(bones.First());

                bones = bones.Select(x => Shift(x, centeredVector));
            }

            if (Settings.minRotate != null && Settings.maxRotate != null)
            {
                var centeredVector = Math.CenteredVector(bones.First());
                if (!Settings.isCenetered)
                {
                    bones = bones.Select(x => Shift(x, centeredVector));
                }

                var rotateAngles = Math.GetRandomVector(Settings.maxRotate,
                                                        Settings.minRotate,
                                                        randomizer.Next());

                bones = bones.Select(x => Rotate(x, Quaternion.Euler(rotateAngles)));

                if (!Settings.isCenetered)
                {
                    bones = bones.Select(x => Shift(x, -centeredVector));
                }
            }

            if (randomizer.NextDouble() < Settings.mirrorRate)
            {
                bones = bones.Select(Mirror);
            }

            var resultList = bones.ToList();
            for (var i = 0; i < resultList.Count - 1; i++)
            {
                if (randomizer.NextDouble() < Settings.addFrameRate)
                {
                    var newFrame = (Vector3[])resultList[i].Clone();
                    for (var j = 0; j < newFrame.Length; j++)
                    {
                        newFrame[j] += resultList[i + 1][j];
                        newFrame[j] /= 2.0f;
                    }
                    resultList.Insert(i + 1, newFrame);
                    i += 1;
                }
            }

            if (Settings.isNormalize)
            {
                return Normalize(resultList.ToArray()).Select(x => gesture[0].Create(x)).ToArray();
            }
            else
            {
                return resultList.Select(x => gesture[0].Create(x)).ToArray();
            }
        }

        #endregion Public Methods

        #region Private Methods

        private Vector3[] Mirror(Vector3[] coords)
        {
            var result = (Vector3[])coords.Clone();
            for (var i = 0; i < coords.Length; i++)
            {
                result[i].x *= -1;
            }
            return result;
        }

        /// <summary>
        /// Добавление одинакового шума в кости
        /// </summary>
        /// <param name="handData"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        private HandData Noise(HandData handData, int seed)
        {
            var randomizer = new System.Random(seed);

            for (var boneId = 1; boneId < handData.Palm.Length; boneId++)
            {
                var noise = Math.GetRandomVector(Settings.maxNoise,
                                                 Settings.minNoise,
                                                 randomizer.Next());

                handData.Palm[boneId] = Math.GetSkeletonRandomVector(handData.Palm[boneId],
                                                                     handData.Palm[boneId - 1],
                                                                     noise);
            }

            for (var fingerId = 0; fingerId < handData.Fingers.Length; fingerId++)
            {
                for (var boneId = 1; boneId < handData.Fingers[fingerId].Length; boneId++)
                {
                    if (handData.Fingers[fingerId].All(v => v.x == 0 && v.y == 0 && v.z == 0))
                    {
                        continue;
                    }

                    var noise = Math.GetRandomVector(Settings.maxNoise,
                                                     Settings.minNoise,
                                                     randomizer.Next());

                    handData.Fingers[fingerId][boneId] = Math.GetSkeletonRandomVector(handData.Fingers[fingerId][boneId],
                                                                                      handData.Fingers[fingerId][boneId - 1],
                                                                                      noise);
                }
            }

            return handData;
        }

        private Vector3[][] Normalize(Vector3[][] gesture)
        {
            var max = 0.0f;
            foreach (var frame in gesture)
            {
                foreach (var b in frame)
                {
                    max = Mathf.Max(Mathf.Abs(b.x), max);
                    max = Mathf.Max(Mathf.Abs(b.y), max);
                    max = Mathf.Max(Mathf.Abs(b.z), max);
                }
            }

            return gesture.AsParallel()
                          .AsOrdered()
                          .Select(frame => frame.Select(b => max != 0.0f ? b / max : b).ToArray())
                          .ToArray();
        }

        /// <summary>
        /// Поворот точек.
        /// </summary>
        /// <param name="coords">
        /// Точки над которыми выполняется поворот.
        /// </param>
        /// <param name="rotateQuat">
        /// Кватерион поворота.
        /// </param>
        /// <returns></returns>
        private Vector3[] Rotate(Vector3[] coords, Quaternion rotateQuat) => (from coord in coords
                                                                              select rotateQuat * coord).ToArray();

        /// <summary>
        /// Аугментация сдвига.
        /// </summary>
        /// <param name="coords">
        /// Точки которые смещаем.
        /// </param>
        /// <param name="shiftVector">
        /// Вектор смещения
        /// </param>
        /// <returns>
        /// Смещеные точки.
        /// </returns>
        private Vector3[] Shift(Vector3[] coords, Vector3 shiftVector) => (from coord in coords
                                                                           select coord + shiftVector).ToArray();

        #endregion Private Methods
    }
}
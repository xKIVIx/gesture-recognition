﻿using Oranges.Data;

namespace Oranges.Logic.Augmentator
{
    /// <summary>
    /// Интерфейс класса, который выполняет аугментацию данных.
    /// </summary>
    internal interface IAugmentator
    {
        #region Public Properties

        /// <summary>
        /// Параметры аугментатора
        /// </summary>
        /// <param name="augmentatorSettings"></param>
        AugmentatorSettings Settings { get; set; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Аугментация данных.
        /// </summary>
        /// <param name="gesture">
        /// Данные жеста.
        /// </param>
        /// <returns>
        /// Аугментированные данные.
        /// </returns>
        HandData[] Augment(HandData[] gesture);

        /// <summary>
        /// Аугментация данных.
        /// </summary>
        /// <param name="gesture">
        /// Данные жеста.
        /// </param>
        /// <param name="seed">
        /// Seed рандомайзера.
        /// </param>
        /// <returns>
        /// Аугментированные данные.
        /// </returns>
        HandData[] Augment(HandData[] gesture, int seed);

        #endregion Public Methods
    }
}
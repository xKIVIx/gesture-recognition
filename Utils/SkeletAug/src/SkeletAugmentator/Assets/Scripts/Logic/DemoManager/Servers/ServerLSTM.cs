﻿using Oranges.Data;
using Oranges.Data.Server;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

namespace Oranges.Logic.DemoManager
{
    internal class ServerLSTM : IServer
    {
        #region Private Enums

        private enum MessageType
        {
            GET_RESULT = 0,
            DATA_BONES = 1,
            NEW_SESSION = 2,
            ERROR_COMPUTE_RESULT = 3,
            SEND_RESULT = 4
        }

        #endregion Private Enums

        #region Public Constructors

        public ServerLSTM()
        {
            IsConnected = false;
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        #endregion Public Constructors

        #region Private Fields

        private bool _isConnected;
        private ServerResult _lastResult;
        private object _lockSocket = new object();
        private Socket _socket;
        private Subject<bool> _subjectIsConnected = new Subject<bool>();
        private Subject<ServerResult> _subjectLastResult = new Subject<ServerResult>();
        private Task _workTask;

        #endregion Private Fields

        #region Public Properties

        public bool IsConnected
        {
            get
            {
                lock (_lockSocket)
                {
                    if (_isConnected)
                    {
                        return _socket.Connected;
                    }
                    else
                    {
                        return _isConnected;
                    }
                }
            }
            private set
            {
                lock (_lockSocket)
                {
                    _isConnected = value;
                    _subjectIsConnected.OnNext(_isConnected);
                }
            }
        }

        public ServerResult LastResult
        {
            get
            {
                lock (_lockSocket)
                {
                    return _lastResult;
                }
            }
            private set
            {
                lock (_lockSocket)
                {
                    _lastResult = value;
                    _subjectLastResult.OnNext(_lastResult);
                }
            }
        }

        #endregion Public Properties

        #region Public Methods

        public ServerErrors ConnectToServer(ConnectData connectData)
        {
            if (IsConnected)
            {
                Disconnect();
            }
            _socket.Connect(connectData.host, connectData.port);
            IsConnected = true;
            _workTask = new Task(WorkThread);
            _workTask.Start();
            return ServerErrors.SUCCESS;
        }

        public ServerErrors Disconnect()
        {
            IsConnected = false;
            lock (_lockSocket)
            {
                _socket.Disconnect(true);
            }

            _workTask.Wait();

            return ServerErrors.SUCCESS;
        }

        public IObservable<bool> ObservableIsConnected() => _subjectIsConnected;

        public IObservable<ServerResult> ObservableLastResult() => _subjectLastResult;

        public ServerErrors SendEndData()
        {
            var message = EncodeMessage(MessageType.GET_RESULT);

            var r = SendMessage(message);

            if (r != ServerErrors.SUCCESS)
            {
                return r;
            }

            message = EncodeMessage(MessageType.NEW_SESSION);

            return SendMessage(message);
        }

        public ServerErrors SendHandData(HandData[] hands)
        {
            var message = EncodeMessage(hands);

            return SendMessage(message);
        }

        #endregion Public Methods

        #region Private Methods

        private int DecodeMessage(byte[] buffer, int offest)
        {
            var messageType = (MessageType)BitConverter.ToInt32(buffer, offest);
            switch (messageType)
            {
                case MessageType.GET_RESULT:
                    break;

                case MessageType.DATA_BONES:
                    break;

                case MessageType.NEW_SESSION:
                    break;

                case MessageType.ERROR_COMPUTE_RESULT:
                    break;

                case MessageType.SEND_RESULT:
                    return 4 + DecodeResult(buffer, offest + 4);
            }
            return buffer.Length;
        }

        private int DecodeResult(byte[] buffer, int offset)
        {
            var size = BitConverter.ToInt32(buffer, offset);

            LastResult = new ServerResult()
            {
                word = Encoding.UTF8.GetString(buffer, offset + 4, size)
            };

            return 4 + size;
        }

        private byte[] EncodeMessage(MessageType type)
        {
            return BitConverter.GetBytes((int)type);
        }

        private byte[] EncodeMessage(HandData[] hands)
        {
            byte[] result = new byte[0];
            foreach (var hand in hands)
            {
                var bones = hand.Bones;
                // count bones * float size in bytes * cont coords + type message bytes + size data in bytes
                var dataSize = bones.Length * 4 * 3;
                var newResult = new byte[result.Length + dataSize + 4 + 4];
                var offset = result.Length;
                Array.Copy(result, newResult, result.Length);
                result = newResult;

                var typeBytes = BitConverter.GetBytes((int)MessageType.DATA_BONES);
                Array.Copy(typeBytes, 0, result, offset, typeBytes.Length);
                offset += typeBytes.Length;

                var dataSizeBytes = BitConverter.GetBytes(dataSize);
                Array.Copy(dataSizeBytes, 0, result, offset, dataSizeBytes.Length);
                offset += dataSizeBytes.Length;

                foreach (var bone in bones)
                {
                    foreach (var coord in new float[] { bone.x, bone.y, bone.z })
                    {
                        var coordBytes = BitConverter.GetBytes(coord);
                        Array.Copy(coordBytes, 0, result, offset, coordBytes.Length);
                        offset += coordBytes.Length;
                    }
                }
            }

            return result;
        }

        private ServerErrors SendMessage(byte[] message)
        {
            if (!IsConnected)
            {
                return ServerErrors.ERROR_DONT_CONNECTED;
            }

            lock (_lockSocket)
            {
                var offset = 0;
                while (offset < message.Length)
                {
                    var send = _socket.Send(message,
                                            offset,
                                            message.Length,
                                            SocketFlags.None);
                    if (send > 0)
                    {
                        offset += send;
                    }
                    else
                    {
                        Debug.LogError("Fail send bones");
                        return ServerErrors.ERROR_UNKNOWN;
                    }
                }
            }

            return ServerErrors.SUCCESS;
        }

        private void WorkThread()
        {
            while (true)
            {
                if (!IsConnected)
                {
                    break;
                }

                byte[] buffer;
                lock (_lockSocket)
                {
                    if (_socket.Available == 0)
                    {
                        continue;
                    }

                    buffer = new byte[_socket.Available];
                    _socket.Receive(buffer, 0, _socket.Available, SocketFlags.None);
                }
                for (var decodedSize = 0;
                    decodedSize < buffer.Length;
                    decodedSize += DecodeMessage(buffer, decodedSize)) ;
            }
        }

        #endregion Private Methods
    }
}
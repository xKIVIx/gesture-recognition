﻿using Oranges.Data;
using Oranges.Data.Server;
using Oranges.Logic.Augmentator;
using System;
using System.Collections.Generic;
using UniRx;

namespace Oranges.Logic.DemoManager
{
    internal class DemoManager : IDemoManager
    {
        #region Public Constructors

        public DemoManager(IServer server,
                           IAugmentator augmentator)
        {
            _server = server;
            _augmentator = augmentator;
        }

        #endregion Public Constructors

        #region Private Fields

        private IServer _server;
        private readonly IAugmentator _augmentator;
        private Subject<bool> _subjectIsConnected = new Subject<bool>();
        private Subject<ServerResult> _subjectLastResult = new Subject<ServerResult>();
        private List<HandData> _dataToSend = new List<HandData>();

        #endregion Private Fields

        #region Public Properties

        public bool IsConnected => _server.IsConnected;
        public ServerResult LastResult => _server.LastResult;
        private bool _isNewSession;

        #endregion Public Properties

        #region Public Methods

        public ServerErrors ConnectToServer(ConnectData connectData)
        {
            var r = _server.ConnectToServer(connectData);
            if (r != ServerErrors.SUCCESS) return r;
            r = _server.SendEndData();
            if (r != ServerErrors.SUCCESS) return r;
            _isNewSession = true;
            return r;
        }

        public ServerErrors Disconnect() => _server.Disconnect();

        public IObservable<bool> ObservableIsConnected() => _server.ObservableIsConnected();

        public IObservable<ServerResult> ObservableLastResult() => _server.ObservableLastResult();

        public ServerErrors SendEndData() => _server.SendEndData();

        public ServerErrors SendHandData(HandData hands)
        {
            if (hands.IsEmpty)
            {
                if(!_isNewSession)
                {
                    _isNewSession = true;
                    var augHands = _augmentator.Augment(_dataToSend.ToArray());
                    _dataToSend = new List<HandData>();
                    _server.SendHandData(augHands);
                    return _server.SendEndData();
                }
                else
                {
                    return ServerErrors.SUCCESS;
                }
                
            }
            else
            {
                _isNewSession = false;
                _dataToSend.Add(hands);
                return ServerErrors.SUCCESS;
            }
        }

        #endregion Public Methods
    }
}
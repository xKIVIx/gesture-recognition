﻿using Oranges.Data;
using Oranges.Data.Server;
using System;

namespace Oranges.Logic.DemoManager
{
    internal interface IServer
    {
        #region Public Properties

        bool IsConnected { get; }
        ServerResult LastResult { get; }

        #endregion Public Properties

        #region Public Methods

        ServerErrors ConnectToServer(ConnectData connectData);

        ServerErrors Disconnect();

        IObservable<bool> ObservableIsConnected();

        IObservable<ServerResult> ObservableLastResult();

        ServerErrors SendEndData();

        ServerErrors SendHandData(HandData[] hands);

        #endregion Public Methods
    }
}
﻿using Oranges.Data;

namespace Oranges.Logic.FileManager
{
    /// <summary>
    /// Возможные возвращаемые ошибки фаловым менеджером.
    /// </summary>
    internal enum FileManagerError
    {
        ERROR_FILE_NOT_FOUND,
        ERROR_UNKNOWN_FILE_EXTENSION,
        SUCCESSED,
        UNKNOWN_ERROR,
        UNSUPPORT_PIXEL_FORMAT
    }

    /// <summary>
    /// Интерфейс фалового менеджера.
    /// </summary>
    internal interface IFileManager
    {
        #region Public Methods

        /// <summary>
        /// Загрузить данные из файла.
        /// </summary>
        /// <param name="path">
        /// Путь до данных
        /// </param>
        /// <returns>
        /// </returns>
        FileManagerError OpenHandData(string path, out HandData[] gestureData);

        /// <summary>
        /// Сохранение данных в файл.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        FileManagerError SaveData(string path, byte[] data);

        /// <summary>
        /// Записать данные в файл.
        /// </summary>
        /// <param name="path">
        /// Путь до данных
        /// </param>
        /// <returns>
        /// </returns>
        FileManagerError SaveHandData(string path, HandData[] gestureData);

        /// <summary>
        /// Сохранение изображения с текстуры.
        /// </summary>
        /// <param name="imageTexture"></param>
        /// <returns></returns>
        FileManagerError SaveImage(string path, ImageData imageTexture);

        #endregion Public Methods
    }
}
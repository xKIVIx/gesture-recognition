﻿using Oranges.Data;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Oranges.Logic.FileManager
{
    internal class ParserMediapipe : IParser
    {
        #region Public Constructors

        public ParserMediapipe()
        {
            _cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            _cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
        }

        #endregion Public Constructors

        #region Private Fields

        private CultureInfo _cultureInfo;

        #endregion Private Fields

        #region Public Methods

        public string ConvertToFileData(HandData[] data)
        {
            throw new System.NotImplementedException();
        }

        public FileManagerError Parse(string path, out HandData[] result)
        {
            if (!File.Exists(path))
            {
                Debug.Log($"ParserMediapipe: File not found. ({path})");
                result = null;
                return FileManagerError.ERROR_FILE_NOT_FOUND;
            }

            var listRows = new List<Vector3[]>();

            using (var fstream = new FileStream(path, FileMode.Open))
            {
                using (var streamReader = new StreamReader(fstream))
                {
                    while (!streamReader.EndOfStream)
                    {
                        var line = streamReader.ReadLine();
                        var splitLine = line.Split(' ');
                        var size = splitLine.Length / 3;
                        var resultRow = new Vector3[size];
                        for (var i = 0; i < size; i++)
                        {
                            resultRow[i] = new Vector3
                            {
                                x = float.Parse(splitLine[i * 3], NumberStyles.Any, _cultureInfo),
                                y = float.Parse(splitLine[i * 3 + 1], NumberStyles.Any, _cultureInfo),
                                z = float.Parse(splitLine[i * 3 + 2], NumberStyles.Any, _cultureInfo)
                            };
                        }
                        listRows.Add(resultRow);
                    }
                }
            }
            result = listRows.Select(x => new HandDataMediapipe { Bones = x }).ToArray();
            return FileManagerError.SUCCESSED;
        }

        #endregion Public Methods
    }
}
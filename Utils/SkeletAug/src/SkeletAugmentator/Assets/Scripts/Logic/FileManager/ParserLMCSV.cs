﻿using Oranges.Data;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

using UnityEngine;

namespace Oranges.Logic.FileManager
{
    internal class ParserLMCSV : IParser
    {
        #region Public Constructors

        public ParserLMCSV()
        {
            _cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            _cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
        }

        #endregion Public Constructors

        #region Private Fields

        private CultureInfo _cultureInfo;

        #endregion Private Fields

        #region Public Methods

        public string ConvertToFileData(HandData[] data)
        {
            var columN = 0;

            var dataVectors = (from d in data
                               select d.Bones).ToArray();

            string header = string.Join(",", (from i in dataVectors[0]
                                              select $"{columN++},{columN++},{columN++}").ToArray());
            var dataString = dataVectors.AsParallel()
                                 .AsOrdered()
                                 .Select(x =>
                                 {
                                     var datas = (from y in x
                                                  select $"{y.x.ToString().Replace(',', '.')},{y.y.ToString().Replace(',', '.')},{y.z.ToString().Replace(',', '.')}")
                                                  .ToArray();
                                     return string.Join(",", datas);
                                 });

            string body = string.Join("\n", dataString);
            return header + "\n" + body;
        }

        public FileManagerError Parse(string path, out HandData[] result)
        {
            if (!File.Exists(path))
            {
                Debug.Log($"ParserLMCSV: File not found. ({path})");
                result = null;
                return FileManagerError.ERROR_FILE_NOT_FOUND;
            }

            var listRows = new List<HandData>();

            using (var fstream = new FileStream(path, FileMode.Open))
            {
                using (var streamReader = new StreamReader(fstream))
                {
                    var headers = streamReader.ReadLine();
                    while (!streamReader.EndOfStream)
                    {
                        var line = streamReader.ReadLine();
                        var splitLine = line.Split(',');
                        var size = splitLine.Length / 3;
                        var resultRow = new Vector3[size];
                        for (var i = 0; i < size; i++)
                        {
                            resultRow[i] = new Vector3
                            {
                                x = float.Parse(splitLine[i * 3], NumberStyles.Any, _cultureInfo),
                                y = float.Parse(splitLine[i * 3 + 1], NumberStyles.Any, _cultureInfo),
                                z = float.Parse(splitLine[i * 3 + 2], NumberStyles.Any, _cultureInfo)
                            };
                        }
                        listRows.Add(new HandDataLeap() { Bones = resultRow });
                        var l = listRows.Last().Bones;
                    }
                }
            }
            result = listRows.ToArray();
            return FileManagerError.SUCCESSED;
        }

        #endregion Public Methods
    }
}
﻿using Oranges.Data;
using System.IO;
using UnityEngine;

namespace Oranges.Logic.FileManager
{
    internal class FileManager : IFileManager
    {
        #region Public Methods

        /// <summary>
        /// </summary>
        /// <see cref="IFileManager.OpenHandData(string)"/>
        public FileManagerError OpenHandData(string path, out HandData[] gestureData)
        {
            IParser parser;

            var fileExtension = Path.GetExtension(path);
            switch (fileExtension)
            {
                case ".csv":
                    {
                        parser = new ParserCSV();
                        break;
                    }
                case ".txt":
                    {
                        parser = new ParserMediapipe();
                        break;
                    }
                case ".lmcsv":
                    {
                        parser = new ParserLMCSV();
                        break;
                    }
                default:
                    {
                        Debug.Log($"FileManager: Unknown file extention. ({fileExtension})");
                        gestureData = null;
                        return FileManagerError.ERROR_UNKNOWN_FILE_EXTENSION;
                    }
            }

            return parser.Parse(path, out gestureData);
        }

        public FileManagerError SaveData(string path, byte[] data)
        {
            File.WriteAllBytes(path, data);
            return FileManagerError.SUCCESSED;
        }

        /// <summary>
        /// <see cref="IFileManager.SaveHandData(string, HandData[])"/>
        /// </summary>
        public FileManagerError SaveHandData(string path, HandData[] gestureData)
        {
            IParser parser;

            var fileExtension = gestureData[0].FILE_EXT;
            switch (fileExtension)
            {
                case "csv":
                    {
                        parser = new ParserCSV();
                        break;
                    }
                case "lmcsv":
                    {
                        parser = new ParserLMCSV();
                        break;
                    }
                default:
                    {
                        Debug.Log($"FileManager: Unknown file extention. ({fileExtension})");
                        return FileManagerError.ERROR_UNKNOWN_FILE_EXTENSION;
                    }
            }

            string fileData = parser.ConvertToFileData(gestureData);
            File.WriteAllText(path, fileData);

            return FileManagerError.SUCCESSED;
        }

        /// <summary>
        /// <see cref="IFileManager.SaveImage(Texture2D)"/>
        /// </summary>
        public FileManagerError SaveImage(string path, ImageData imageData)
        {
            byte[] fileData;
            switch (imageData.format)
            {
                case TextureFormat.R8:
                    fileData = JPEGTurbo.Encode(imageData.bytes, (uint)imageData.width, (uint)imageData.height, JPEGTurbo.PixelType.GRAY, 8);
                    break;

                case TextureFormat.R16:
                    fileData = JPEGTurbo.Encode(imageData.bytes, (uint)imageData.width, (uint)imageData.height, JPEGTurbo.PixelType.GRAY, 16);
                    break;

                case TextureFormat.RGBA32:
                    fileData = JPEGTurbo.Encode(imageData.bytes, (uint)imageData.width, (uint)imageData.height, JPEGTurbo.PixelType.RGBA, 8);
                    break;

                default:

                    return FileManagerError.UNSUPPORT_PIXEL_FORMAT;
            }
            File.WriteAllBytes(path, fileData);
            return FileManagerError.SUCCESSED;
        }

        #endregion Public Methods
    }
}
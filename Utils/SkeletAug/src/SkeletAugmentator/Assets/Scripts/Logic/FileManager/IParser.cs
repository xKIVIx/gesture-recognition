﻿using Oranges.Data;

namespace Oranges.Logic.FileManager
{
    /// <summary>
    /// Интерфейс парсеров файлов
    /// </summary>
    internal interface IParser
    {
        #region Public Methods

        /// <summary>
        /// Препобразование данных в формат для записи.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        string ConvertToFileData(HandData[] data);

        /// <summary>
        /// Парсинг файла.
        /// </summary>
        /// <param name="path">
        /// Путь до файла. (Абсолютный)
        /// </param>
        /// <param name="result">
        /// Результат парсинга. Содержит список фреймов, каждый из которых
        /// представляет список координат костей руки.
        /// </param>
        /// <returns></returns>
        FileManagerError Parse(string path, out HandData[] result);

        #endregion Public Methods
    }
}
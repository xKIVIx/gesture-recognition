﻿using Oranges.Logic.Augmentator;
using Oranges.Logic.DemoManager;
using Oranges.Logic.DevicesManager;
using Oranges.Logic.FileManager;
using UnityEngine;
using Zenject;

namespace Oranges.Logic
{
    [CreateAssetMenu(fileName = "LogicScriptableObjectsInstaller", menuName = "Installers/Logic")]
    internal class LogicZenjectInstaller : ScriptableObjectInstaller
    {
        #region Public Methods

        public override void InstallBindings()
        {
            Container.Bind<MainManager>().AsSingle().NonLazy();
            Container.Bind<IFileManager>().To<FileManager.FileManager>().AsSingle();
            Container.Bind<IAugmentator>().To<Augmentator.Augmentator>().AsSingle();
            Container.Bind<IDevicesManager>().To<DevicesManager.DevicesManager>().AsSingle();
            Container.Bind<IDemoManager>().To<DemoManager.DemoManager>().AsSingle();
            Container.Bind<IServer>().To<ServerLSTM>().AsTransient();
        }

        #endregion Public Methods
    }
}
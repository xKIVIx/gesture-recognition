﻿using Oranges.Data;
using System;
using UniRx;

namespace Oranges.Logic.DevicesManager
{
    internal interface IDevicesManager
    {
        #region Public Properties

        /// <summary>
        /// Список подключеных устройств.
        /// </summary>
        ReactiveCollection<DeviceInfo> DevicesInfos { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Запуск устройства записи.
        /// </summary>
        void StartDevice(int id);

        void StartRecord(Action<File<ImageData>> saveImageFunction,
                         Action<File<HandData[]>> saveSkeletFunction);

        /// <summary>
        /// Остановка устройства записи.
        /// </summary>
        void StopDevice(int id);

        /// <summary>
        /// Остановка всех устройств.
        /// </summary>
        void StopDevices();

        void StopRecord();

        void StartRecognize();

        void StopRecognize();

        /// <summary>
        /// Считывание текущего фрейма с устройств.
        /// </summary>
        void СaptureFrame();

        #endregion Public Methods
    }
}
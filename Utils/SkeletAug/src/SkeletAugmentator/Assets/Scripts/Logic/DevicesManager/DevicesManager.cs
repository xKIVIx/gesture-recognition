﻿using Oranges.Data;
using Oranges.Devices;
using Oranges.Logic.Augmentator;
using Oranges.Logic.DemoManager;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace Oranges.Logic.DevicesManager
{
    internal class DevicesManager : IDevicesManager
    {
        #region Private Structs

        private struct DeviceHistory
        {
            #region Public Fields

            public List<HandData> _skelets;

            #endregion Public Fields
        }

        #endregion Private Structs

        #region Private Constructors

        private DevicesManager(IDevice[] devices,
                               IDemoManager demoManager)
        {
            foreach (var device in devices)
            {
                AddDevice(device);
            }

            _demoManager = demoManager;
        }

        #endregion Private Constructors

        #region Private Destructors

        ~DevicesManager()
        {
            StopDevices();
        }

        #endregion Private Destructors

        #region Private Fields

        private List<IDevice> _devices = new List<IDevice>();
        private bool _isRecordStart = false;
        private int _recorderedFrame = 0;
        private Action<File<ImageData>> _saveImageFunction;
        private Action<File<HandData[]>> _saveSkeletFunction;
        private DeviceHistory[] _skeletsHistory;
        private bool _isRecognizeStart = false;
        private readonly IDemoManager _demoManager;

        #endregion Private Fields

        #region Public Properties

        public ReactiveCollection<DeviceInfo> DevicesInfos { get; private set; } = new ReactiveCollection<DeviceInfo>();

        #endregion Public Properties

        #region Public Methods

        public void StartDevice(int id) => _devices[id].Start();

        public void StartRecord(Action<File<ImageData>> saveImageFunction,
                                Action<File<HandData[]>> saveSkeletFunction)
        {
            _skeletsHistory = new DeviceHistory[_devices.Count];
            for (var i = 0; i < _skeletsHistory.Length; i++)
            {
                var deviceInfo = _devices[i].Info.Value;

                _skeletsHistory[i]._skelets = new List<HandData>();
            }
            _recorderedFrame = 0;
            _saveImageFunction = saveImageFunction;
            _saveSkeletFunction = saveSkeletFunction;
            _isRecordStart = true;
        }

        public void StopDevice(int id) => _devices[id].Stop();

        public void StopDevices()
        {
            foreach (var device in _devices)
            {
                device.Stop();
            }
        }

        public void StopRecord()
        {
            for (int deviceId = 0; deviceId < _skeletsHistory.Length; deviceId++)
            {
                var deviceHistory = _skeletsHistory[deviceId];
                if (deviceHistory._skelets.Count == 0)
                    continue;
                var file = new File<HandData[]>
                {
                    name = _devices[deviceId].Info.Value.title,
                    data = deviceHistory._skelets.ToArray()
                };
                file.name += "_Skelet";
                file.ext = file.data[0].FILE_EXT;
                _saveSkeletFunction(file);
            }

            _recorderedFrame = 0;
            _saveImageFunction = null;
            _saveSkeletFunction = null;
            _isRecordStart = false;
        }

        public void СaptureFrame()
        {
            for (int deviceId = 0; deviceId < _devices.Count; deviceId++)
            {
                var device = _devices[deviceId];
                var state = device.СaptureFrame();
                if (state == DeviceErrors.SUCCESSED)
                {
                    if (_isRecordStart)
                    {
                        var deviceError = device.GetRawLastFrames(out ImageData[] rawFrames);
                        int typeId = 0;
                        foreach (var frame in rawFrames)
                        {
                            var file = new File<ImageData>
                            {
                                name = $"{device.Info.Value.title}_{typeId}_{_recorderedFrame}",
                                ext = "jpg",
                                data = frame
                            };
                            _saveImageFunction(file);
                            typeId++;
                        }

                        if (device.Info.Value.skelets != null)
                        {
                            var skelet = device.Info.Value.skelets.Value;
                            _skeletsHistory[deviceId]._skelets.Add(skelet);
                        }
                    }

                    if (device.Info.Value.skelets != null && _isRecognizeStart)
                    {
                        var skelet = device.Info.Value.skelets.Value;

                        if (_isRecognizeStart)
                        {
                            _demoManager.SendHandData(skelet);
                        }
                    }
                }
            }

            if (_isRecordStart)
            {
                _recorderedFrame++;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void AddDevice(IDevice recorder)
        {
            int deviceId = _devices.Count;
            _devices.Add(recorder);
            DevicesInfos.Add(recorder.Info.Value);
            var dispos = recorder.Info
                                 .Subscribe(x =>
                                 {
                                     DevicesInfos[deviceId] = x;
                                 });

            DevicesInfos.ObserveRemove()
                        .Where(x =>
                        {
                            return x.Index == deviceId;
                        })
                        .Subscribe(x =>
                        {
                            dispos.Dispose();
                        });
        }

        public void StartRecognize()
        {
            _isRecognizeStart = true;
        }

        public void StopRecognize()
        {
            _isRecognizeStart = false;
        }

        #endregion Private Methods
    }
}
﻿using Oranges.Data;
using Oranges.Logic.Augmentator;
using Oranges.Logic.DemoManager;
using Oranges.Logic.DevicesManager;
using Oranges.Logic.FileManager;
using Oranges.UI;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

namespace Oranges.Logic
{
    internal class MainManager
    {
        #region Public Constructors

        public MainManager(IFileManager fileManager,
                           IUIManager uIManager,
                           IAugmentator augmentator,
                           IDevicesManager devicesManager,
                           IDemoManager demoManager)
        {
            _fileManager = fileManager;
            _uIManager = uIManager;
            _augmentator = augmentator;
            _devicesManager = devicesManager;
            _demoManager = demoManager;
            SubscribeDeviceManager();
            _uIManager.Start
                      .Subscribe(x => SubscribeUI());
            Observable.OnceApplicationQuit()
                      .Subscribe(x => { _devicesManager.StopDevices(); });
        }

        #endregion Public Constructors

        #region Private Fields

        private const uint MAX_COUNT_TASKS = 20;
        private readonly IAugmentator _augmentator;
        private readonly IDevicesManager _devicesManager;
        private readonly IDemoManager _demoManager;
        private readonly IFileManager _fileManager;
        private readonly IUIManager _uIManager;
        private int _countRecordFramePerSec = 0;
        private IDisposable _disposableCaputerFrames;
        private HandData[] _gestureData;
        private bool _isRecordStart = false;
        private ConcurrentBag<File<ImageData>> _saveData;
        private List<IObservable<Unit>> _tasks;

        #endregion Private Fields

        #region Private Methods

        private void AugmentateFiles(string[] files)
        {
            var countAugSteps = _augmentator.Settings.countEpoch * files.Length;

            var counterComplite = new IntReactiveProperty(0);
            counterComplite.Subscribe(x => _uIManager.ProcentLoad = (float)x / (float)countAugSteps * (float)100);
            _uIManager.IsViewLoadscreen = true;
            Observable.Start(() =>
            {
                var randomizer = new System.Random(_augmentator.Settings.seed);
                foreach (var filePath in files)
                {
                    var exampleNumb = Path.GetFileNameWithoutExtension(
                                           Path.GetDirectoryName(filePath));
                    var gestureName = Path.GetFileNameWithoutExtension(
                                          Path.GetDirectoryName(
                                              Path.GetDirectoryName(filePath)));
                    var fileName = Path.GetFileNameWithoutExtension(filePath);
                    var fileExt = Path.GetExtension(filePath);
                    var saveDir = $"{_uIManager.SavePathAugFiles}/{gestureName}/{exampleNumb}";
                    Directory.CreateDirectory(saveDir);
                    for (var augEpoch = 0; augEpoch < _augmentator.Settings.countEpoch; augEpoch++)
                    {
                        var savePath = $"{saveDir}/{fileName}_{augEpoch}.{fileExt}";
                        HandData[] datas;
                        _fileManager.OpenHandData(filePath, out datas);
                        _fileManager.SaveHandData(savePath, _augmentator.Augment(datas, randomizer.Next()));
                        counterComplite.Value++;
                    }
                }

                File.WriteAllText($"{_uIManager.SavePathAugFiles}/settings.json",
                                  JsonUtility.ToJson(_augmentator.Settings));
            })
            .SubscribeOnMainThread()
            .Subscribe(x => _uIManager.IsViewLoadscreen = false);
        }

        private void OpenHandData(string path)
        {
            if (_fileManager.OpenHandData(path, out _gestureData) == FileManagerError.SUCCESSED)
            {
                _augmentator.Augment(_gestureData);
                _uIManager.GestureOriginal.Value = _gestureData;
                _uIManager.GestureAugment.Value = _gestureData;
            }
        }

        private void RecordImage(Data.File<ImageData> file)
        {
            string path = $"{_uIManager.SavePathRecordFiles}" +
                          $"/{_uIManager.TitleRecordGesture}" +
                          $"/{_uIManager.CountRecordedExamples}";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            _saveData.Add(file);
            if (_tasks.Count < MAX_COUNT_TASKS)
            {
                var task = new Task((state) =>
                {
                    while (_saveData.TryTake(out File<ImageData> imageData) || _isRecordStart)
                    {
                        _fileManager.SaveImage((string)state + $"/{imageData.name}.{imageData.ext}", imageData.data);
                    }
                }, path);
                task.Start();
                _tasks.Add(task.ToObservable());
            }
        }

        private void RecordSkelet(Data.File<HandData[]> file)
        {
            string path = $"{_uIManager.SavePathRecordFiles}" +
                          $"/{_uIManager.TitleRecordGesture}" +
                          $"/{_uIManager.CountRecordedExamples}";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path += $"/{file.name}.{file.ext}";
            _fileManager.SaveHandData(path, file.data);
        }

        private void SetRecordFrameRate(int frameRate)
        {
            _disposableCaputerFrames?.Dispose();
            _disposableCaputerFrames = Observable.EveryUpdate()
                                                 .TakeUntil(Observable.OnceApplicationQuit())
                                                 .Sample(TimeSpan.FromMilliseconds(1000 / frameRate))
                                                 .Subscribe(x =>
                                                 {
                                                     _devicesManager.СaptureFrame();
                                                     _countRecordFramePerSec++;
                                                 });
        }

        private void StartRecord()
        {
            _tasks = new List<IObservable<Unit>>();
            _uIManager.CountRecordedExamples.Value += 1;
            _uIManager.IsRecordReady = false;
            _saveData = new ConcurrentBag<File<ImageData>>();
            _devicesManager.StartRecord(RecordImage, RecordSkelet);
            _isRecordStart = true;
        }

        private void StopRecord()
        {
            _isRecordStart = false;
            _devicesManager.StopRecord();
            Observable.WhenAll(_tasks)
                      .ObserveOnMainThread()
                      .Subscribe(x =>
                      {
                          _uIManager.IsRecordReady = true;
                      });
        }

        private void SubscribeDeviceManager()
        {
            _devicesManager.DevicesInfos
                           .ObserveAdd()
                           .Subscribe(x => _uIManager.RecordDiveces.Insert(x.Index, x.Value));

            _devicesManager.DevicesInfos
                           .ObserveReplace()
                           .Subscribe(x => _uIManager.RecordDiveces[x.Index] = x.NewValue);

            _devicesManager.DevicesInfos
                           .ObserveReset()
                           .Subscribe(x => _uIManager.RecordDiveces.Clear());
        }

        private void SubscribeUI()
        {
            _uIManager.PathOpenedFile
                      .Where(x => x != null)
                      .Subscribe(OpenHandData);

            _uIManager.AugmentatorSettings
                      .Subscribe(x =>
                      {
                          _augmentator.Settings = x;
                          if (_gestureData != null)
                          {
                              _uIManager.GestureAugment.Value = _augmentator.Augment(_gestureData);
                          }
                      });

            _uIManager.ObservableAugmentFiles
                      .Subscribe(AugmentateFiles);

            _uIManager.ObservableStartDevice
                      .Subscribe(x =>
                      {
                          if (!_devicesManager.DevicesInfos[x].isStarted)
                          {
                              _devicesManager.StartDevice(x);
                          }
                          else
                          {
                              _devicesManager.StopDevice(x);
                          }
                      });

            _uIManager.ObservableStartRecord
                      .Subscribe(x =>
                      {
                          if (_isRecordStart)
                          {
                              StopRecord();
                          }
                          else
                          {
                              StartRecord();
                          }
                      });

            _uIManager.FrameRateRecord
                      .Subscribe(SetRecordFrameRate);

            _uIManager.IsRecordReady = true;
            foreach (var devInfo in _devicesManager.DevicesInfos)
            {
                _uIManager.RecordDiveces.Add(devInfo);
            }

            Observable.Timer(TimeSpan.FromSeconds(1))
                      .RepeatSafe()
                      .Subscribe(x =>
                      {
                          _uIManager.CurrentFrameRateRecord.Value = _countRecordFramePerSec;
                          _countRecordFramePerSec = 0;
                      });

            _uIManager.ObservableDemoServerConnect()
                      .Subscribe(x =>
                      { 
                          if ( _demoManager.ConnectToServer(x) == Data.Server.ServerErrors.SUCCESS)
                          {
                              _devicesManager.StartRecognize();
                          }
                      });

            _uIManager.ObservableDemoServerDisconnect()
                      .Subscribe(x =>
                      {
                          _devicesManager.StopRecognize();
                          _demoManager.Disconnect();
                      });

            _demoManager.ObservableIsConnected()
                        .Subscribe(x =>
                        {
                            _uIManager.DemoServerIsConnected.Value = x;
                        });

            _demoManager.ObservableLastResult()
                        .Subscribe(x => {
                            _uIManager.DemoServerResult.Value = x.word;
                            });
        }

        #endregion Private Methods
    }
}
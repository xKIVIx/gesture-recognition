﻿namespace Oranges.Data.Server
{
    public enum ServerErrors
    {
        SUCCESS,
        ERROR_SERVER_DONT_ANSWER,
        ERROR_DONT_CONNECTED,
        ERROR_UNKNOWN
    }
}
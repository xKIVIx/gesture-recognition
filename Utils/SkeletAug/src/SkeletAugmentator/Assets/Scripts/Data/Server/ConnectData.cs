﻿namespace Oranges.Data.Server
{
    public struct ConnectData
    {
        #region Public Fields

        public string host;
        public int port;

        #endregion Public Fields
    }
}
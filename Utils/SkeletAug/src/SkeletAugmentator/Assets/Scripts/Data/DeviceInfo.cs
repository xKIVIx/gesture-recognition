﻿using UniRx;

using UnityEngine;

namespace Oranges.Data
{
    public struct DeviceInfo
    {
        #region Public Fields

        public bool isConnected;
        public bool isStarted;
        public Texture2D[] outputTextures;
        public ReactiveProperty<HandData> skelets;
        public string title;

        #endregion Public Fields
    }
}
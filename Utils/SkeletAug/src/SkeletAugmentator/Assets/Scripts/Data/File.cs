﻿namespace Oranges.Data
{
    public struct File<DataType>
    {
        #region Public Fields

        public DataType data;
        public string ext;
        public string name;

        #endregion Public Fields
    }
}
﻿using System;

using UnityEngine;

namespace Oranges.Data
{
    [Serializable]
    public struct AugmentatorSettings
    {
        #region Public Fields

        public double addFrameRate;
        public short countBones;

        public int countEpoch;

        public bool isCenetered;

        public bool isNormalize;

        public Vector3 maxShift,
                       minShift,
                       maxRotate,
                       minRotate,
                       maxNoise,
                       minNoise;

        public double mirrorRate;

        public int seed;

        #endregion Public Fields
    }
}
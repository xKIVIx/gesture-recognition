﻿using System;
using System.Linq;
using UnityEngine;

namespace Oranges.Data
{
    public class HandDataLeap : HandData
    {
        #region Public Fields

        public const int COUNT_BONES = 20;

        #endregion Public Fields

        #region Private Fields

        private readonly int[,] _handMap = new int[,] { { 0, 1, 2, 3 },
                                                        { 4, 5, 6, 7 },
                                                        { 8, 9, 10, 11 },
                                                        { 12, 13, 14, 15 },
                                                        { 16, 17, 18, 19 } };

        #endregion Private Fields

        #region Public Properties

        public override Vector3[] Bones
        {
            get
            {
                var result = new Vector3[COUNT_BONES * CountHands];
                var idHand = 0;

                if (Fingers == null)
                {
                    return result;
                }

                for (var idFing = 0; idFing < Fingers.Length; idFing++)
                {
                    if ((idHand + 1) * 5 <= idFing)
                    {
                        idHand++;
                    }
                    for (var idFingBone = 0; idFingBone < Fingers[idFing].Length; idFingBone++)
                    {
                        var id = _handMap[idFing % 5, idFingBone] + idHand * COUNT_BONES;
                        result[id] = Fingers[idFing][idFingBone];
                    }
                }

                return result;
            }

            set
            {
                var bones = value;
                if (bones.Length % COUNT_BONES != 0)
                    throw new IndexOutOfRangeException($"Count bones must be N % {COUNT_BONES} == 0!");

                IsEmpty = bones.All(v => v.x == 0 && v.y == 0 && v.z == 0);
                CountHands = bones.Length / COUNT_BONES;

                Palm = new Vector3[0];

                Fingers = new Vector3[CountHands * 5][];

                for (var idHand = 0; idHand < CountHands; idHand++)
                {
                    for (var fing = 0; fing < 5; fing++)
                    {
                        var idFing = (idHand * 5) + fing;
                        Fingers[idFing] = new Vector3[_handMap.GetLength(1)];

                        for (var idFingBone = 0; idFingBone < Fingers[idFing].Length; idFingBone++)
                        {
                            var id = _handMap[fing, idFingBone] + COUNT_BONES * idHand;
                            Fingers[idFing][idFingBone] = bones[id];
                        }
                    }
                }
            }
        }

        public override string FILE_EXT => "lmcsv";

        #endregion Public Properties

        #region Public Methods

        public override object Clone()
        {
            return new HandDataLeap
            {
                Fingers = Fingers.Select(x => x.Select(y => y).ToArray()).ToArray(),
                Palm = Palm.Select(x => x).ToArray(),
                IsEmpty = IsEmpty,
                CountHands = CountHands
            };
        }

        public override HandData Create(Vector3[] bones) => new HandDataLeap { Bones = bones };

        #endregion Public Methods
    }
}
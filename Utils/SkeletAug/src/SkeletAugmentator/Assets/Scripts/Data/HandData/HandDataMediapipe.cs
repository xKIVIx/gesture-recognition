﻿using System;
using System.Linq;
using UnityEngine;

namespace Oranges.Data
{
    public class HandDataMediapipe : HandData
    {
        #region Private Fields

        private readonly int[][] _handMap = new int[][] { new int[]{ 0, 1, 2, 3, 4 },
                                                          new int[]{ 0, 5, 6, 7, 8 },
                                                          new int[]{ 0, 9, 10, 11, 12 },
                                                          new int[]{ 0, 13, 14, 15, 16 },
                                                          new int[]{ 0, 17, 18, 19, 20 } };

        #endregion Private Fields

        #region Public Properties

        public override Vector3[] Bones
        {
            get
            {
                var result = new Vector3[21];
                for (var idFing = 0; idFing < Fingers.Length; idFing++)
                {
                    for (var idFingBone = 0; idFingBone < Fingers[idFing].Length; idFingBone++)
                    {
                        var id = _handMap[idFing][idFingBone];
                        result[id] = Fingers[idFing][idFingBone];
                    }
                }

                return result;
            }

            set
            {
                var bones = value;

                if (bones.Length != 21)
                    throw new IndexOutOfRangeException("Count bones must be 21!");

                IsEmpty = bones.All(v => v.x == 0 && v.y == 0 && v.z == 0);
                CountHands = 1;

                Palm = new Vector3[0];

                Fingers = new Vector3[5][];

                for (var idFing = 0; idFing < Fingers.Length; idFing++)
                {
                    Fingers[idFing] = new Vector3[_handMap[idFing].Length];

                    for (var idFingBone = 0; idFingBone < Fingers[idFing].Length; idFingBone++)
                    {
                        var id = _handMap[idFing][idFingBone];
                        Fingers[idFing][idFingBone] = bones[id];
                    }
                }
            }
        }

        public override string FILE_EXT => "txt";

        #endregion Public Properties

        #region Public Methods

        public override object Clone()
        {
            return new HandDataMediapipe
            {
                Fingers = Fingers.Select(x => x.Select(y => y).ToArray()).ToArray(),
                Palm = Palm.Select(x => x).ToArray(),
                IsEmpty = IsEmpty,
                CountHands = CountHands
            };
        }

        public override HandData Create(Vector3[] bones) => new HandDataMediapipe { Bones = bones };

        #endregion Public Methods
    }
}
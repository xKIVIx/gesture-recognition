﻿using System;
using System.Linq;
using UnityEngine;

namespace Oranges.Data
{
    public class HandDataIntel : HandData
    {
        #region Private Fields

        private readonly int[][] _handMap = new int[][] { new int[]{ 0, 1 },
                                                          new int[]{ 0, 2, 3, 4, 5 },
                                                          new int[]{ 1, 6, 7, 8, 9 },
                                                          new int[]{ 1, 10, 11, 12, 13 },
                                                          new int[]{ 1, 14, 15, 16, 17 },
                                                          new int[]{ 1, 18, 19, 20, 21 } };

        #endregion Private Fields

        #region Public Properties

        public override Vector3[] Bones
        {
            get
            {
                var result = new Vector3[22];

                for (var idBone = 0; idBone < Palm.Length; idBone++)
                {
                    var id = _handMap[0][idBone];
                    result[id] = Palm[idBone];
                }

                for (var idFing = 0; idFing < Fingers.Length; idFing++)
                {
                    for (var idFingBone = 0; idFingBone < Fingers[idFing].Length; idFingBone++)
                    {
                        var id = _handMap[idFing + 1][idFingBone];
                        result[id] = Fingers[idFing][idFingBone];
                    }
                }

                return result;
            }

            set
            {
                var bones = value;

                if (bones.Length != 22)
                    throw new IndexOutOfRangeException("Count bones must be 22!");

                IsEmpty = bones.All(v => v.x == 0 && v.y == 0 && v.z == 0);
                CountHands = 1;

                Palm = new Vector3[_handMap[0].Length];

                for (var i = 0; i < _handMap[0].Length; i++)
                {
                    Palm[i] = bones[_handMap[0][i]];
                }

                Fingers = new Vector3[5][];

                for (var idFing = 0; idFing < 5; idFing++)
                {
                    Fingers[idFing] = new Vector3[_handMap[idFing + 1].Length];

                    for (var idFingBone = 0; idFingBone < Fingers[idFing].Length; idFingBone++)
                    {
                        var id = _handMap[idFing + 1][idFingBone];
                        Fingers[idFing][idFingBone] = bones[id];
                    }
                }
            }
        }

        public override string FILE_EXT => "csv";

        #endregion Public Properties

        #region Public Methods

        public override object Clone()
        {
            return new HandDataIntel
            {
                Fingers = Fingers.Select(x => x.Select(y => y).ToArray()).ToArray(),
                Palm = Palm.Select(x => x).ToArray(),
                IsEmpty = IsEmpty,
                CountHands = CountHands
            };
        }

        public override HandData Create(Vector3[] bones) => new HandDataIntel { Bones = bones };

        #endregion Public Methods
    }
}
﻿using System;
using UnityEngine;

namespace Oranges.Data
{
    public abstract class HandData : ICloneable
    {
        #region Public Properties

        public abstract Vector3[] Bones { get; set; }
        public int CountHands { get; protected set; }
        public abstract string FILE_EXT { get; }
        public Vector3[][] Fingers { get; protected set; }

        public bool IsEmpty { get; protected set; } = true;
        public Vector3[] Palm { get; protected set; }

        #endregion Public Properties

        #region Public Methods

        public abstract object Clone();

        public abstract HandData Create(Vector3[] bones);

        #endregion Public Methods
    }
}
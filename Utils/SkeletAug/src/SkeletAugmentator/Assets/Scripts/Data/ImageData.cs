﻿using UnityEngine;

namespace Oranges.Data
{
    public struct ImageData
    {
        #region Public Fields

        public byte[] bytes;
        public TextureFormat format;
        public int width, height;

        #endregion Public Fields
    }
}
﻿using Oranges.UI.Controller;
using UniRx;
using UnityEngine;

using Zenject;

namespace Oranges.UI
{
    internal class UIZenjectInstaller : MonoInstaller
    {
#pragma warning disable CS0649

        #region Private Fields

        [SerializeField] private AugmentatorController _augmentatorController;
        [SerializeField] private CamersController _camersController;
        [SerializeField] private DemoController _demoController;
        [SerializeField] private FileController _fileController;
        [SerializeField] private Loadscreen _loadscreen;
        private Subject<Unit> _observableStart = new Subject<Unit>();

        [Header("Controllers")]
        [SerializeField] private RecordController _recordController;

        [SerializeField] private RenderController _renderController;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Methods

        public override void InstallBindings()
        {
            Container.BindInstance<IRenderController>(_renderController).AsSingle();
            Container.BindInstance<IFileController>(_fileController).AsSingle();
            Container.BindInstance<IAugmentatorController>(_augmentatorController).AsSingle();
            Container.BindInstance<ILoadscreen>(_loadscreen).AsSingle();
            Container.BindInstance<ICamersController>(_camersController).AsSingle();
            Container.BindInstance<IRecordController>(_recordController).AsSingle();
            Container.BindInstance<IDemoController>(_demoController).AsSingle();
            Container.Bind<IUIManager>().To<UIManager>().AsSingle().WithArguments(_observableStart);
        }

        public override void Start()
        {
            base.Start();

            _observableStart.OnNext(new Unit());
        }

        #endregion Public Methods
    }
}
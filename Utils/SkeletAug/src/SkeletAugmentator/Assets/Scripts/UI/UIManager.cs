﻿using Oranges.Data;
using Oranges.Data.Server;
using Oranges.UI.Controller;
using System;
using System.Linq;
using UniRx;

namespace Oranges.UI
{
    internal class UIManager : IUIManager
    {
        #region Public Constructors

        public UIManager(IRenderController renderController,
                         IFileController fileController,
                         IAugmentatorController augmentatorController,
                         ICamersController camersController,
                         ILoadscreen loadscreen,
                         IRecordController recordController,
                         IDemoController demoController,
                         IObservable<Unit> _initedUI)
        {
            _renderController = renderController;
            _fileController = fileController;
            _augmentatorController = augmentatorController;
            _loadscreen = loadscreen;
            _camersController = camersController;
            _recordController = recordController;
            _demoController = demoController;
            Start = _initedUI;
            _initedUI.Subscribe(x => _demoController.AugmentSettings.Subscribe(
                s => _augmentatorController.Settings.Value = s
                ));

        }

        #endregion Public Constructors

        #region Private Fields

        private IAugmentatorController _augmentatorController;
        private ICamersController _camersController;
        private IDemoController _demoController;
        private IFileController _fileController;
        private ILoadscreen _loadscreen;
        private IRecordController _recordController;
        private IRenderController _renderController;

        #endregion Private Fields

        #region Public Properties

        public ReactiveProperty<AugmentatorSettings> AugmentatorSettings => _augmentatorController.Settings;

        public IntReactiveProperty CountRecordedExamples => _recordController.CurrentGestureExample;

        public IntReactiveProperty CurrentFrameRateRecord => _recordController.CurrentFrameRate;
        public BoolReactiveProperty DemoServerIsConnected => _demoController.IsConnected;
        public StringReactiveProperty DemoServerResult => _demoController.OutputResult;
        public IntReactiveProperty FrameRateRecord => _recordController.FrameRate;

        public ReactiveProperty<HandData[]> GestureAugment => _renderController.GestureAugment;

        public ReactiveProperty<HandData[]> GestureOriginal => _renderController.GestureOriginal;

        public bool IsRecordReady { get => _recordController.IsReadyRecord; set => _recordController.IsReadyRecord = value; }

        public bool IsViewLoadscreen { get => _loadscreen.IsView.Value; set => _loadscreen.IsView.Value = value; }

        public IObservable<string[]> ObservableAugmentFiles => _augmentatorController.ObservableAugmentFiles()
                                                                                    .Where(x => SavePathAugFiles != "")
                                                                                    .Select(x => _fileController.AugmentedFiles.ToArray());

        public IObservable<int> ObservableStartDevice => _camersController.ObservableStartDevice;

        public IObservable<Unit> ObservableStartRecord => _recordController.ObservableStartRecord;

        public StringReactiveProperty PathOpenedFile => _fileController.PathOpenedFile;

        public float ProcentLoad { get => _loadscreen.Procent.Value; set => _loadscreen.Procent.Value = value; }

        public ReactiveCollection<DeviceInfo> RecordDiveces => _camersController.DeviceInfos;

        public string SavePathAugFiles => _fileController.SavePathAug;

        public StringReactiveProperty SavePathRecordFiles => _recordController.SavePath;

        public IObservable<Unit> Start { get; private set; }
        public StringReactiveProperty TitleRecordGesture => _recordController.GestureTitle;

        #endregion Public Properties

        #region Public Methods

        public IObservable<ConnectData> ObservableDemoServerConnect() => _demoController.ObservableConnect();

        public IObservable<Unit> ObservableDemoServerDisconnect() => _demoController.ObservableDisconnect();

        #endregion Public Methods
    }
}
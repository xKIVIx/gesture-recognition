﻿using Oranges.Data;
using Oranges.UI.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Oranges.UI.Controller
{
    internal class CamersController : ObservableTriggerBase, ICamersController
    {
        #region Private Structs

        private struct Camera
        {
            #region Public Constructors

            public Camera(DeviceInfo deviceInfo,
                          Action<DeviceInfo> startDeviceCommand,
                          CameraSatus cameraStatusPrefab,
                          CameraPreview cameraPreviewPrefab,
                          RenderHand renderHandPrefab,
                          Transform[] statusLists,
                          Transform[] previewsLists)
            {
                _countPreviewsLists = previewsLists.Length;

                _status = new CameraSatus[statusLists.Length];
                for (var i = 0; i < statusLists.Length; i++)
                {
                    _status[i] = Instantiate(cameraStatusPrefab, statusLists[i]);
                    _status[i].DeviceInfo = deviceInfo;
                    _status[i].GetObservableStartButton()
                              .Subscribe(startDeviceCommand);
                }

                _imagesPreviews = new CameraPreview[previewsLists.Length *
                                                    deviceInfo.outputTextures.Length];
                if (deviceInfo.skelets != null)
                {
                    _renderHands = new RenderHand[previewsLists.Length];
                    deviceInfo.skelets
                              .Where(x => x != null)
                              .SubscribeOnMainThread()
                              .Subscribe(UpdatePreviewSkelets);
                }
                else
                {
                    _renderHands = new RenderHand[0];
                }

                for (var listId = 0; listId < previewsLists.Length; listId++)
                {
                    for (var imageId = 0; imageId < deviceInfo.outputTextures.Length; imageId++)
                    {
                        var i = listId * previewsLists.Length + imageId;
                        _imagesPreviews[i] = Instantiate(cameraPreviewPrefab, previewsLists[listId]);
                        _imagesPreviews[i].SetTexture(deviceInfo.outputTextures[imageId]);
                    }

                    if (deviceInfo.skelets != null)
                    {
                        _renderHands[listId] = Instantiate(renderHandPrefab, previewsLists[listId]);
                    }
                }
            }

            #endregion Public Constructors

            #region Private Fields

            private int _countPreviewsLists;
            private CameraPreview[] _imagesPreviews;
            private RenderHand[] _renderHands;
            private CameraSatus[] _status;

            #endregion Private Fields

            #region Public Methods

            public void Destroy()
            {
                if (_status != null)
                {
                    foreach (var s in _status)
                    {
                        GameObject.Destroy(s.gameObject);
                    }

                    _status = new CameraSatus[0];
                }

                if (_imagesPreviews != null)
                {
                    foreach (var p in _imagesPreviews)
                    {
                        GameObject.Destroy(p.gameObject);
                    }
                    _imagesPreviews = new CameraPreview[0];
                }

                if (_renderHands != null)
                {
                    foreach (var p in _renderHands)
                    {
                        GameObject.Destroy(p.gameObject);
                    }
                    _renderHands = new RenderHand[0];
                }
            }

            #endregion Public Methods

            #region Private Methods

            private void UpdatePreviewSkelets(HandData hands)
            {
                if (hands != null)
                {
                    var centerVector = Utils.Math.CenteredVector(hands.Bones);

                    for (var listId = 0; listId < _countPreviewsLists; listId++)
                    {
                        if (_renderHands[listId].isActiveAndEnabled)
                        {
                            _renderHands[listId].RendHand(hands, centerVector);
                        }
                    }
                }
            }

            #endregion Private Methods
        }

        #endregion Private Structs

#pragma warning disable CS0649

        #region Private Fields

        [SerializeField] private CameraPreview _cameraPreviewPrefab;

        [Header("Prefabs")]
        [SerializeField] private CameraSatus _cameraStatusPrefab;

        private List<Camera> _camers = new List<Camera>();

        [Header("Lists")]
        [SerializeField] private Transform[] _previewsLists;

        [SerializeField] private RenderHand _skeletPreviwPrefab;
        [SerializeField] private Transform[] _statusLists;
#pragma warning restore CS0649
        private Subject<int> _subjectStartDevice = new Subject<int>();

        #endregion Private Fields

        #region Public Properties

        public ReactiveCollection<DeviceInfo> DeviceInfos { get; private set; } = new ReactiveCollection<DeviceInfo>();

        public IObservable<int> ObservableStartDevice => _subjectStartDevice;

        #endregion Public Properties

        #region Protected Methods

        protected override void RaiseOnCompletedOnDestroy() => _subjectStartDevice.OnCompleted();

        #endregion Protected Methods

        #region Private Methods

        private void AddDevice(CollectionAddEvent<DeviceInfo> collectionAddEvent) => _camers.Insert(collectionAddEvent.Index,
                           new Camera(collectionAddEvent.Value,
                                      HandleStartButton,
                                      _cameraStatusPrefab,
                                      _cameraPreviewPrefab,
                                      _skeletPreviwPrefab,
                                      _statusLists,
                                      _previewsLists));

        private void HandleStartButton(DeviceInfo deviceInfo)
        {
            for (int i = 0; i < DeviceInfos.Count; i++)
            {
                if (DeviceInfos[i].title == deviceInfo.title)
                {
                    _subjectStartDevice.OnNext(i);
                    break;
                }
            }
        }

        private void RemoveDevice(CollectionRemoveEvent<DeviceInfo> collectionRemoveEvent)
        {
            _camers[collectionRemoveEvent.Index].Destroy();
            _camers.RemoveAt(collectionRemoveEvent.Index);
        }

        private void ReplaceDevice(CollectionReplaceEvent<DeviceInfo> collectionReplaceEvent)
        {
            _camers[collectionReplaceEvent.Index].Destroy();
            _camers[collectionReplaceEvent.Index] = new Camera(collectionReplaceEvent.NewValue,
                                                               HandleStartButton,
                                                               _cameraStatusPrefab,
                                                               _cameraPreviewPrefab,
                                                               _skeletPreviwPrefab,
                                                               _statusLists,
                                                               _previewsLists);
        }

        private void ResetDevices(Unit unit)
        {
            foreach (var cam in _camers)
            {
                cam.Destroy();
            }

            _camers.Clear();
        }

        private void Start()
        {
            DeviceInfos.ObserveAdd()
                       .ObserveOnMainThread()
                       .Subscribe(AddDevice);

            DeviceInfos.ObserveReplace()
                       .ObserveOnMainThread()
                       .Subscribe(ReplaceDevice);

            DeviceInfos.ObserveRemove()
                       .ObserveOnMainThread()
                       .Subscribe(RemoveDevice);

            DeviceInfos.ObserveReset()
                       .ObserveOnMainThread()
                       .Subscribe(ResetDevices);
        }

        #endregion Private Methods
    }
}
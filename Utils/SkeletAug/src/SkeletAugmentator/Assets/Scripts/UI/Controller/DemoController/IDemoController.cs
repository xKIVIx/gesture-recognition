﻿using Oranges.Data;
using Oranges.Data.Server;
using System;
using UniRx;

namespace Oranges.UI.Controller
{
    internal interface IDemoController
    {
        #region Public Properties

        BoolReactiveProperty IsConnected { get; }

        StringReactiveProperty OutputResult { get; }

        #endregion Public Properties

        #region Public Methods

        IObservable<ConnectData> ObservableConnect();

        IObservable<Unit> ObservableDisconnect();

        ReactiveProperty<AugmentatorSettings> AugmentSettings { get; }

        #endregion Public Methods
    }
}
﻿using SFB;
using System.IO;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Controller
{
    /// <summary>
    /// <see cref="IFileController.PathOpenedFile"/>
    /// </summary>
    internal class FileController : MonoBehaviour, IFileController
    {
        #region Private Fields

        /// <summary>
        /// Поддерживаемые расширения файлов.
        /// </summary>
        private readonly string[] SUPPORT_EXTENSIONS = new string[] { "csv", "txt", "lmcsv" };

#pragma warning disable CS0649

        /// <summary>
        /// Кнопка для добавления папки с данными на аугментацию
        /// </summary>
        [SerializeField]
        private Button _buttonAddFolderToAug;

        /// <summary>
        /// Кнопка для открытия данных.
        /// </summary>
        [SerializeField]
        private Button _buttonOpen;

        /// <summary>
        /// Кнопка для открытия окна с выбором файла.
        /// </summary>
        [SerializeField]
        private Button _buttonOpenFileBrowser;

        /// <summary>
        /// Кнопка выбора пути по которому будут сохранены аугментированые файлы.
        /// </summary>
        [SerializeField]
        private Button _buttonSelectSavePathAug;

        /// <summary>
        /// Патерн подгружаемых файлов для аугментации
        /// </summary>
        [SerializeField]
        private TMP_InputField _inputFilesPattern;

        /// <summary>
        /// Ввод пути для сохранения.
        /// </summary>
        [SerializeField]
        private TMP_InputField _inputPathSaveAug;

        /// <summary>
        /// Список объектов представляющих на UI файлы для аугментации.
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _listFileAugView;

        /// <summary>
        /// Input поле для ввода пути где лежат данные.
        /// </summary>
        [SerializeField]
        private TMP_InputField _pathToData;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        /// <summary>
        /// <see cref="IFileController.AugmentedFiles"/>
        /// </summary>
        public ReactiveCollection<string> AugmentedFiles { get; private set; } = new ReactiveCollection<string>();

        /// <summary>
        /// <see cref="IFileController.PathOpenedFile"/>
        /// </summary>
        public StringReactiveProperty PathOpenedFile { get; private set; } = new StringReactiveProperty();

        /// <summary>
        /// <see cref="IFileController.SavePathAug"/>
        /// </summary>
        public string SavePathAug => _inputPathSaveAug.text;

        #endregion Public Properties

        #region Private Methods

        /// <summary>
        /// Поиск файлов для аугментации в каталоге и под каталогах.
        /// </summary>
        /// <param name="path"></param>
        private void AddAugFolder(string path)
        {
            var files = Directory.GetFiles(path, _inputFilesPattern.text);
            foreach (var file in files)
            {
                AugmentedFiles.Add(file);
            }

            var subDirs = Directory.GetDirectories(path);
            foreach (var subDir in subDirs)
            {
                AddAugFolder(subDir);
            }
        }

        /// <summary>
        /// Открытие диалогового окна выбора файла.
        /// </summary>
        /// <param name="unit"></param>
        private void OnOpenFileDialog(Unit unit)
        {
            var extensions = new ExtensionFilter[] {
                new ExtensionFilter {
                    Extensions = SUPPORT_EXTENSIONS,
                    Name = "CSV"
                }
            };
            var file = StandaloneFileBrowser.OpenFilePanel("Select file", "", extensions, false);
            if (file.Length != 0)
            {
                _pathToData.text = file[0];
            }
        }

        /// <summary>
        /// Открытие диалогового окна для выбора паки содержащей файлы для аугментации.
        /// </summary>
        /// <param name="unit"></param>
        private void OnSelectAugFolder(Unit unit)
        {
            var folders = StandaloneFileBrowser.OpenFolderPanel("Select folders", "", true);
            foreach (var folder in folders)
            {
                AddAugFolder(folder);
            }
        }

        private void Start()
        {
            AugmentedFiles.ObserveAdd()
                          .Where(x => x.Index < AugmentedFiles.Count - 1)
                          .Subscribe(x => _listFileAugView.text = string.Join("\n", AugmentedFiles));

            AugmentedFiles.ObserveAdd()
                          .Where(x => x.Index == AugmentedFiles.Count - 1)
                          .Subscribe(x => _listFileAugView.text += $"\n{x.Value}");

            AugmentedFiles.ObserveRemove()
                          .Subscribe(x => _listFileAugView.text = string.Join("\n", AugmentedFiles));

            AugmentedFiles.ObserveReset()
                          .Subscribe(x => _listFileAugView.text = "");

            AugmentedFiles.ObserveReplace()
                          .Subscribe(x => _listFileAugView.text = string.Join("\n", AugmentedFiles));

            AugmentedFiles.ObserveMove()
                          .Subscribe(x => _listFileAugView.text = string.Join("\n", AugmentedFiles));

            _buttonAddFolderToAug.OnClickAsObservable()
                                 .Subscribe(OnSelectAugFolder);

            _buttonOpen.OnClickAsObservable()
                       .Subscribe(x => { PathOpenedFile.Value = _pathToData.text; });

            _buttonOpenFileBrowser.OnClickAsObservable()
                                  .Subscribe(OnOpenFileDialog);

            _buttonSelectSavePathAug.OnClickAsObservable()
                                    .Subscribe(x =>
                                    {
                                        var folder = StandaloneFileBrowser.OpenFolderPanel("Select folders",
                                                                                           "",
                                                                                           false);
                                        if (folder.Length != 0)
                                        {
                                            _inputPathSaveAug.text = folder[0];
                                        }
                                    });
        }

        #endregion Private Methods
    }
}
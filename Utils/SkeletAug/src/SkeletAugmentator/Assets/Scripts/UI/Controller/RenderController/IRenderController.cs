﻿using Oranges.Data;
using UniRx;

namespace Oranges.UI.Controller
{
    /// <summary>
    /// Компонент интерфейса отвечающий за рендер руки на экране.
    /// </summary>
    internal interface IRenderController
    {
        #region Public Properties

        /// <summary>
        /// Аугментированые данные жеста.
        /// </summary>
        ReactiveProperty<HandData[]> GestureAugment { get; }

        /// <summary>
        /// Оригинальные данные жеста.
        /// </summary>
        ReactiveProperty<HandData[]> GestureOriginal { get; }

        #endregion Public Properties
    }
}
﻿using TMPro;

using UniRx;

using UnityEngine;

namespace Oranges.UI.Controller
{
    public class Loadscreen : MonoBehaviour, ILoadscreen
    {
#pragma warning disable CS0649

        #region Private Fields

        [SerializeField]
        private TextMeshProUGUI _procentText;

        [SerializeField]
        private GameObject _screenObject;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        public BoolReactiveProperty IsView { get; private set; } = new BoolReactiveProperty();
        public FloatReactiveProperty Procent { get; private set; } = new FloatReactiveProperty();

        #endregion Public Properties

        #region Private Methods

        private void Start()
        {
            Procent.ObserveOnMainThread()
                   .Subscribe(x => _procentText.text = $"{x}%");

            IsView.ObserveOnMainThread()
                  .Subscribe(_screenObject.SetActive);

            IsView.Value = false;
        }

        #endregion Private Methods
    }
}
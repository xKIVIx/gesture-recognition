﻿using System;

using UniRx;

namespace Oranges.UI.Controller
{
    internal interface IRecordController
    {
        #region Public Properties

        IntReactiveProperty CurrentFrameRate { get; }

        /// <summary>
        /// Текущий записываемый пример жеста.
        /// </summary>
        IntReactiveProperty CurrentGestureExample { get; }

        /// <summary>
        /// Частота кадров записи.
        /// </summary>
        IntReactiveProperty FrameRate { get; }

        /// <summary>
        /// Название жеста.
        /// </summary>
        StringReactiveProperty GestureTitle { get; }

        /// <summary>
        /// Готова ли ситема к записи.
        /// </summary>
        bool IsReadyRecord { get; set; }

        /// <summary>
        /// Нажатие клавиши начала записи.
        /// </summary>
        IObservable<Unit> ObservableStartRecord { get; }

        /// <summary>
        /// Корневая папка сохранения записей.
        /// </summary>
        StringReactiveProperty SavePath { get; }

        #endregion Public Properties
    }
}
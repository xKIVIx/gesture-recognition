﻿using Oranges.Data;
using Oranges.UI.Components;
using Oranges.Utils;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Controller
{
    /// <summary>
    /// <see cref="IRenderController"/>
    /// </summary>
    public class RenderController : MonoBehaviour, IRenderController
    {
#pragma warning disable CS0649

        #region Private Fields

        /// <summary>
        /// Кнопка паузы анимации.
        /// </summary>
        [SerializeField]
        private Button _buttonPause;

        /// <summary>
        /// Кнопка запуска анимации.
        /// </summary>
        [SerializeField]
        private Button _buttonPlay;

        /// <summary>
        /// Кнопка остановки анимации.
        /// </summary>
        [SerializeField]
        private Button _buttonStop;

        /// <summary>
        /// Вектор для централизации изображения.
        /// </summary>
        private Vector3 _centeredVector;

        /// <summary>
        /// Текущий отображаемый фрейм.
        /// </summary>
        private IntReactiveProperty _currentFrame = new IntReactiveProperty();

        /// <summary>
        /// Оригинальные данные для отрисовки.
        /// </summary>
        private HandData[] _handDatas;

        /// <summary>
        /// Агументированые данные отрисовки.
        /// </summary>
        private HandData[] _handDatasAug;

        /// <summary>
        /// Отрисовкщик аугментированых данных
        /// </summary>
        [SerializeField]
        private RenderHand _renderHandAugment;

        /// <summary>
        /// Отрисовкщик оригинальных данных
        /// </summary>
        [SerializeField]
        private RenderHand _renderHandOriginal;

        /// <summary>
        /// Слайдер показывающий текущий выбраный фрейм.
        /// </summary>
        [SerializeField]
        private Slider _sliderSelectFrame;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        /// <summary>
        /// <see cref="IRenderController.GestureAugment"/>
        /// </summary>
        public ReactiveProperty<HandData[]> GestureAugment { get; private set; } = new ReactiveProperty<HandData[]>();

        /// <summary>
        /// <see cref="IRenderController.GestureOriginal"/>
        /// </summary>
        public ReactiveProperty<HandData[]> GestureOriginal { get; private set; } = new ReactiveProperty<HandData[]>();

        #endregion Public Properties

        #region Private Methods

        /// <summary>
        /// Обработчик обновления аугментированых данных жеста.
        /// </summary>
        /// <param name="handDatas">
        /// Новые данные жеста.
        /// </param>
        private void HandleUpdateGestureAugment(HandData[] handDatas)
        {
            _handDatasAug = handDatas;
            _currentFrame.SetValueAndForceNotify(_currentFrame.Value);
        }

        /// <summary>
        /// Обработчик обновления оригинальных данных жеста.
        /// </summary>
        /// <param name="handDatas">
        /// Новые данные жеста.
        /// </param>
        private void HandleUpdateGestureOriginal(HandData[] handDatas)
        {
            _centeredVector = Math.CenteredVector(handDatas[0].Bones);
            _handDatas = handDatas;
            _currentFrame.SetValueAndForceNotify(0);
            _sliderSelectFrame.maxValue = handDatas.Length - 1;
        }

        // Start is called before the first frame update
        private void Start()
        {
            GestureOriginal.Where(x => x?.Length > 0)
                           .Subscribe(HandleUpdateGestureOriginal);

            GestureAugment.Where(x => x?.Length > 0)
                          .Subscribe(HandleUpdateGestureAugment);

            _currentFrame.Where(x => GestureOriginal.Value?.Length > x)
                         .Subscribe(x =>
                         {
                             _renderHandOriginal.RendHand(_handDatas[x], _centeredVector);
                             _sliderSelectFrame.value = x;
                         });

            _currentFrame.Where(x => GestureAugment.Value?.Length > x)
                         .Subscribe(x =>
                         {
                             _renderHandAugment.RendHand(_handDatasAug[x], _centeredVector);
                             _sliderSelectFrame.value = x;
                         });

            _currentFrame.Where(x => (GestureOriginal.Value?.Length <= x) && (x != 0))
                         .Subscribe(x => { _currentFrame.Value = 0; });

            _buttonStop.OnClickAsObservable()
                       .Subscribe(x => { _currentFrame.Value = 0; });

            _sliderSelectFrame.OnValueChangedAsObservable()
                              .Subscribe(x => { _currentFrame.Value = (int)x; });

            Observable.EveryLateUpdate()
                      .SampleFrame(1)
                      .SkipUntil(_buttonPlay.OnClickAsObservable())
                      .TakeUntil(_buttonPause.OnClickAsObservable())
                      .TakeUntil(_buttonStop.OnClickAsObservable())
                      .RepeatUntilDestroy(this)
                      .Subscribe(x => { _currentFrame.Value += 1; });
        }

        #endregion Private Methods
    }
}
﻿using Oranges.Data.Server;
using Oranges.Data;
using Oranges.UI.Components;
using System;
using System.Linq;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Controller
{
    internal class DemoController : MonoBehaviour, IDemoController
    {
        #region Private Fields
#pragma warning disable CS0649
        [SerializeField] private Button _buttonConnect;

        [Header("Buttons")]
        [SerializeField] private Button _buttonDisconnect;

        [Header("Inputs")]
        [SerializeField] private TMP_InputField _inputHostame;

        [SerializeField] private TMP_InputField _inputPort;

        [Header("Outputs")]
        [SerializeField] private TMP_Text _outputResult;

        [Header("Status")]
        [SerializeField] private StatusSwaper _statusConnect;

        [Header("Toggles")]
        [SerializeField] private Toggle _toggleCentered;
        [SerializeField] private Toggle _toggleNormalize;
#pragma warning restore CS0649

        #endregion Private Fields

        #region Public Properties

        public BoolReactiveProperty IsConnected { get; private set; } = new BoolReactiveProperty(false);

        public StringReactiveProperty OutputResult { get; private set; } = new StringReactiveProperty();

        public ReactiveProperty<AugmentatorSettings> AugmentSettings { get; private set; } = new ReactiveProperty<AugmentatorSettings>();

        #endregion Public Properties

        #region Public Methods

        public IObservable<ConnectData> ObservableConnect() => _buttonConnect.OnClickAsObservable()
                                                                             .Select(x => new ConnectData()
                                                                             {
                                                                                 host = _inputHostame.text,
                                                                                 port = int.Parse(_inputPort.text)
                                                                             });

        public IObservable<Unit> ObservableDisconnect() => _buttonDisconnect.OnClickAsObservable();

        #endregion Public Methods

        #region Private Methods

        private void Start()
        {
            IsConnected.SubscribeOnMainThread()
                       .Subscribe(x => _statusConnect.IsTrue = x);

            OutputResult.ObserveOnMainThread()
                        .Subscribe(x => _outputResult.text = x);

            _toggleCentered.OnValueChangedAsObservable()
                           .Subscribe(x =>
                           {
                               var settings = new AugmentatorSettings
                               {
                                   isCenetered = x,
                                   isNormalize = this.AugmentSettings.Value.isNormalize
                               };

                               this.AugmentSettings.Value = settings;
                           });

            _toggleCentered.OnValueChangedAsObservable()
                           .Subscribe(x =>
                           {
                               var settings = new AugmentatorSettings
                               {
                                   isCenetered = this.AugmentSettings.Value.isCenetered,
                                   isNormalize = x
                               };

                               this.AugmentSettings.Value = settings;
                           });
        }

        #endregion Private Methods
    }
}
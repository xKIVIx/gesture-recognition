﻿using UniRx;

namespace Oranges.UI.Controller
{
    /// <summary>
    /// Компонент интерфейса отвечающий за выбор открываемого файла.
    /// </summary>
    internal interface IFileController
    {
        #region Public Properties

        /// <summary>
        /// Пути к файлам, которые должны быть аугментированы.
        /// </summary>
        ReactiveCollection<string> AugmentedFiles { get; }

        /// <summary>
        /// Путь текущего выбранго файла.
        /// </summary>
        StringReactiveProperty PathOpenedFile { get; }

        /// <summary>
        /// Путь для сохранения.
        /// </summary>
        string SavePathAug { get; }

        #endregion Public Properties
    }
}
﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Oranges.UI.Controller
{
    public class RenderViewController : MonoBehaviour,
                                       IDragHandler,
                                       IScrollHandler
    {
#pragma warning disable CS0649

        #region Private Fields

        /// <summary>
        /// Чувствительность перемещения камеры.
        /// </summary>
        [SerializeField]
        private float _moveSensevity = 0.01f;

        /// <summary>
        /// Чувствительность перемещения камеры.
        /// </summary>
        [SerializeField]
        private float _rotateSensevity = 0.1f;

        /// <summary>
        /// Компонент над которым происходят трансофрмации.
        /// </summary>
        [SerializeField]
        private RectTransform _rotateTransform;

        /// <summary>
        /// Чувствительность скрола.
        /// </summary>
        [SerializeField]
        private float _scrollSensevity = 10.0f;

        /// <summary>
        /// Компонент над которым происходят трансофрмации сдвига.
        /// </summary>
        [SerializeField]
        private RectTransform _shiftTransform;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Methods

        public void OnDrag(PointerEventData eventData)
        {
            var data = new Vector3(eventData.delta.x, eventData.delta.y, 0);
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                data *= _rotateSensevity;
                _rotateTransform.Rotate(new Vector3(data.y, data.x, 0));
            }
            else
            {
                data *= _moveSensevity;
                _shiftTransform.Translate(data);
            }
        }

        public void OnScroll(PointerEventData eventData)
        {
            var d = _scrollSensevity * eventData.scrollDelta.y;
            _rotateTransform.localScale += new Vector3
            {
                x = d,
                y = d,
                z = d
            };
        }

        #endregion Public Methods

        #region Private Methods

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
        }

        #endregion Private Methods
    }
}
﻿using Oranges.UI.Components;
using SFB;
using System;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Controller
{
    internal class RecordController : ObservableTriggerBase, IRecordController
    {
        #region Private Fields
#pragma warning disable CS0649
        [SerializeField] private Button _buttonSelectSavePath;

        [SerializeField] private TMP_InputField _inputExampleCount;

        [SerializeField] private TMP_InputField _inputFrameRate;

        [SerializeField] private TMP_InputField _inputGestureTitle;

        [SerializeField] private TMP_InputField _inputSavePath;

        /// <summary>
        /// Код клавиши на которую начинается запись.
        /// </summary>
        [SerializeField] private KeyCode _keyCodeStartRecord;

        [SerializeField] private TMP_Text _outputCurrentFPS;

        /// <summary>
        /// Отображение статуса готовности к записи.
        /// </summary>
        [SerializeField] private StatusSwaper _statusReady;

        /// <summary>
        /// Отображение текущего стоятояния включения записи.
        /// </summary>
        [SerializeField] private StatusSwaper _statusRecord;
#pragma warning restore CS0649

        private Subject<Unit> _subjectPressStartRecord = new Subject<Unit>();

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Текущая частота записи.
        /// </summary>
        public IntReactiveProperty CurrentFrameRate { get; } = new IntReactiveProperty(0);

        /// <summary>
        /// <see cref="IRecordController.CurrentGestureExample"/>
        /// </summary>
        public IntReactiveProperty CurrentGestureExample { get; } = new IntReactiveProperty();

        public IntReactiveProperty FrameRate { get; } = new IntReactiveProperty(20);

        /// <summary>
        /// <see cref="IRecordController.GestureTitle"/>
        /// </summary>
        public StringReactiveProperty GestureTitle { get; } = new StringReactiveProperty();

        /// <summary>
        /// <see cref="IRecordController.IsReadyRecord"/>
        /// </summary>
        public bool IsReadyRecord { get => _statusReady.IsTrue; set => _statusReady.IsTrue = value; }

        /// <summary>
        /// <see cref="IRecordController.ObservableStartRecord"/>
        /// </summary>
        public IObservable<Unit> ObservableStartRecord => _subjectPressStartRecord;

        /// <summary>
        /// <see cref="IRecordController.SavePath"/>
        /// </summary>
        public StringReactiveProperty SavePath { get; } = new StringReactiveProperty();

        #endregion Public Properties

        #region Protected Methods

        protected override void RaiseOnCompletedOnDestroy() => _subjectPressStartRecord.OnCompleted();

        #endregion Protected Methods

        #region Private Methods

        private void HandleSelectSavePath(Unit unit)
        {
            var savePath = StandaloneFileBrowser.OpenFolderPanel("Select save folder", null, false);
            if (savePath?.Length > 0)
            {
                SavePath.Value = savePath[0];
            }
        }

        private void Start()
        {
            ObservableStartRecord.Subscribe(x => _statusRecord.IsTrue = !_statusRecord.IsTrue);

            CurrentGestureExample.SubscribeOnMainThread()
                                 .Subscribe(x => _inputExampleCount.text = x.ToString());

            GestureTitle.SubscribeOnMainThread()
                        .Subscribe(x => _inputGestureTitle.text = x);

            SavePath.SubscribeOnMainThread()
                    .Subscribe(x => _inputSavePath.text = x);

            FrameRate.SubscribeOnMainThread()
                     .Subscribe(x => _inputFrameRate.text = x.ToString());

            CurrentFrameRate.SubscribeOnMainThread()
                            .Subscribe(x => _outputCurrentFPS.text = x.ToString());

            _buttonSelectSavePath.OnClickAsObservable()
                                 .Subscribe(HandleSelectSavePath);

            _inputSavePath.onValueChanged
                          .AsObservable()
                          .Subscribe(x => SavePath.Value = x);

            _inputGestureTitle.onValueChanged
                              .AsObservable()
                              .Subscribe(x => GestureTitle.Value = x);

            _inputFrameRate.onValueChanged
                           .AsObservable()
                           .Subscribe(x => FrameRate.Value = int.Parse(x));

            _inputExampleCount.onValueChanged
                              .AsObservable()
                              .Subscribe(x => CurrentGestureExample.Value = int.Parse(x));
        }

        private void Update()
        {
            if (Input.GetKeyDown(_keyCodeStartRecord) && (IsReadyRecord || _statusRecord.IsTrue))
            {
                _subjectPressStartRecord.OnNext(new Unit());
            }
        }

        #endregion Private Methods
    }
}
﻿using UniRx;

namespace Oranges.UI.Controller
{
    public interface ILoadscreen
    {
        #region Public Properties

        BoolReactiveProperty IsView { get; }
        FloatReactiveProperty Procent { get; }

        #endregion Public Properties
    }
}
﻿using Oranges.Data;
using System;
using UniRx;

namespace Oranges.UI.Controller
{
    internal interface ICamersController
    {
        #region Public Properties

        ReactiveCollection<DeviceInfo> DeviceInfos { get; }

        IObservable<int> ObservableStartDevice { get; }

        #endregion Public Properties
    }
}
﻿using Oranges.Data;
using Oranges.UI.Components;
using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Controller
{
    /// <summary>
    /// Компонент предоставляющий управление аугментацией.
    /// </summary>
    internal class AugmentatorController : MonoBehaviour, IAugmentatorController
    {
#pragma warning disable CS0649

        #region Private Fields

        /// <summary>
        /// Кнопка зпуска аугментации файлов.
        /// </summary>
        [SerializeField]
        private Button _buttonAugmentFiles;

        /// <summary>
        /// Вероятность добавление нового кадра
        /// </summary>
        [SerializeField]
        private TMP_InputField _inputAddFrameRate;

        /// <summary>
        /// Количество эпох аугментации файлов.
        /// </summary>
        [SerializeField]
        private TMP_InputField _inputCountEpoch;

        /// <summary>
        /// Вероятность зеркального отображения.
        /// </summary>
        [SerializeField]
        private TMP_InputField _inputMirrorRate;

        /// <summary>
        /// Рандомный сид.
        /// </summary>
        [SerializeField]
        private TMP_InputField _inputRandomSeed;

        /// <summary>
        /// Переключатель централизации данных.
        /// </summary>
        [SerializeField]
        private Toggle _toggleIsCentered;

        /// <summary>
        /// Переключатель нормализации данных.
        /// </summary>
        [SerializeField]
        private Toggle _toggleIsNormalize;

        /// <summary>
        /// Параметры аугментации
        /// </summary>
        [SerializeField]
        private VectorInput _vectorShiftMin,
                                      _vectorShiftMax,
                                      _vectorRotateMin,
                                      _vectorRotateMax,
                                      _vectorNoiseMin,
                                      _vectorNoiseMax;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        public ReactiveProperty<AugmentatorSettings> Settings { get; private set; } =
            new ReactiveProperty<AugmentatorSettings>(new AugmentatorSettings());

        #endregion Public Properties

        #region Public Methods

        public IObservable<Unit> ObservableAugmentFiles() => _buttonAugmentFiles.OnClickAsObservable();

        #endregion Public Methods

        #region Private Methods

        private void Start()
        {
            _inputAddFrameRate.onValueChanged
                              .AsObservable()
                              .Subscribe(x =>
                              {
                                  var s = Settings.Value;
                                  s.addFrameRate = double.Parse(x);
                                  Settings.Value = s;
                              });

            _inputMirrorRate.onValueChanged
                            .AsObservable()
                            .Subscribe(x =>
                            {
                                var s = Settings.Value;
                                s.mirrorRate = double.Parse(x);
                                Settings.Value = s;
                            });

            _inputRandomSeed.onValueChanged
                            .AsObservable()
                            .Subscribe(x =>
                            {
                                var s = Settings.Value;
                                s.seed = int.Parse(x);
                                Settings.Value = s;
                            });

            _inputCountEpoch.onValueChanged
                            .AsObservable()
                            .Subscribe(x =>
                            {
                                var s = Settings.Value;
                                s.countEpoch = int.Parse(x);
                                Settings.Value = s;
                            });

            _toggleIsCentered.OnValueChangedAsObservable()
                             .Subscribe(x =>
                             {
                                 var s = Settings.Value;
                                 s.isCenetered = x;
                                 Settings.Value = s;
                             });

            _toggleIsNormalize.OnValueChangedAsObservable()
                             .Subscribe(x =>
                             {
                                 var s = Settings.Value;
                                 s.isNormalize = x;
                                 Settings.Value = s;
                             });

            _vectorShiftMin.Value
                           .AsObservable()
                           .Subscribe(x =>
                           {
                               var s = Settings.Value;
                               s.minShift = x;
                               Settings.Value = s;
                           });

            _vectorShiftMax.Value
                           .AsObservable()
                           .Subscribe(x =>
                           {
                               var s = Settings.Value;
                               s.maxShift = x;
                               Settings.Value = s;
                           });

            _vectorRotateMin.Value
                            .AsObservable()
                            .Subscribe(x =>
                            {
                                var s = Settings.Value;
                                s.minRotate = x;
                                Settings.Value = s;
                            });

            _vectorRotateMax.Value
                            .AsObservable()
                            .Subscribe(x =>
                            {
                                var s = Settings.Value;
                                s.maxRotate = x;
                                Settings.Value = s;
                            });

            _vectorNoiseMin.Value
                           .AsObservable()
                           .Subscribe(x =>
                           {
                               var s = Settings.Value;
                               s.minNoise = x;
                               Settings.Value = s;
                           });

            _vectorNoiseMax.Value
                           .AsObservable()
                           .Subscribe(x =>
                           {
                               var s = Settings.Value;
                               s.maxNoise = x;
                               Settings.Value = s;
                           });
            _inputCountEpoch.text = "1";
            var randomizer = new System.Random();
            _inputRandomSeed.text = randomizer.Next().ToString();
        }

        #endregion Private Methods
    }
}
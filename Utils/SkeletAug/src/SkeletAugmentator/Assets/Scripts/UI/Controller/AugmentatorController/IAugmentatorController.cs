﻿using Oranges.Data;
using System;
using UniRx;

namespace Oranges.UI.Controller
{
    internal interface IAugmentatorController
    {
        #region Public Properties

        /// <summary>
        /// Параметры аугментации данных.
        /// </summary>
        ReactiveProperty<AugmentatorSettings> Settings { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Получение IObservable для задания логики при
        /// вызов аугментации данных из списка файлов.
        /// </summary>
        /// <returns></returns>
        IObservable<Unit> ObservableAugmentFiles();

        #endregion Public Methods
    }
}
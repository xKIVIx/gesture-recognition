﻿using System.Globalization;

using TMPro;

using UniRx;

using UnityEngine;

namespace Oranges.UI.Components
{
    public class VectorInput : MonoBehaviour
    {
#pragma warning disable CS0649

        #region Private Fields

        [SerializeField]
        private TMP_InputField _x;

        [SerializeField]
        private TMP_InputField _y;

        [SerializeField]
        private TMP_InputField _z;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        public Vector3ReactiveProperty Value { get; private set; } = new Vector3ReactiveProperty();

        #endregion Public Properties

        #region Private Methods

        // Start is called before the first frame update
        private void Start()
        {
            var ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";

            _x.onValueChanged.AddListener(x =>
            {
                if (x.Length > 0)
                {
                    var v = Value.Value;
                    v.x = float.Parse(x, NumberStyles.Any, ci);
                    Value.Value = v;
                }
                else
                {
                    _x.text = "0";
                }
            });

            _y.onValueChanged.AddListener(y =>
            {
                if (y.Length > 0)
                {
                    var v = Value.Value;
                    v.y = float.Parse(y, NumberStyles.Any, ci);
                    Value.Value = v;
                }
                else
                {
                    _y.text = "0";
                }
            });

            _z.onValueChanged.AddListener(z =>
            {
                if (z.Length > 0)
                {
                    var v = Value.Value;
                    v.z = float.Parse(z, NumberStyles.Any, ci);
                    Value.Value = v;
                }
                else
                {
                    _z.text = "0";
                }
            });
        }

        #endregion Private Methods
    }
}
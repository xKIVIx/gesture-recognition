﻿using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Components
{
    internal class CameraPreview : MonoBehaviour
    {
#pragma warning disable CS0649

        #region Private Fields

        [SerializeField] private AspectRatioFitter _aspectRatioFitter;
        [SerializeField] private RawImage _rawImage;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Methods

        public void SetTexture(Texture texture)
        {
            _aspectRatioFitter.aspectRatio = (float)texture.width / (float)texture.height;
            _rawImage.texture = texture;
        }

        #endregion Public Methods
    }
}
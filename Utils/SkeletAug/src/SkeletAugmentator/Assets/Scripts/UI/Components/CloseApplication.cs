﻿using UniRx;

using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Components
{
    [RequireComponent(typeof(Button))]
    internal class CloseApplication : MonoBehaviour
    {
        #region Private Methods

        private void Start()
        {
            var button = GetComponent<Button>();
            button.OnClickAsObservable()
                  .Subscribe(x =>
                  {
                      Application.Quit();
                  });
        }

        #endregion Private Methods
    }
}
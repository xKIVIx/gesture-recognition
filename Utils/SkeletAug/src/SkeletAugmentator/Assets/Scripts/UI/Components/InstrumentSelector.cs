﻿using System;
using System.Linq;

using UniRx;

using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Components
{
    [RequireComponent(typeof(Dropdown))]
    internal class InstrumentSelector : MonoBehaviour
    {
        #region Private Structs

        [Serializable]
        private struct Instrument
        {
#pragma warning disable CS0649

            #region Public Fields

            public string title;

            public GameObject ui;

            #endregion Public Fields

#pragma warning restore CS0649
        }

        #endregion Private Structs

        #region Private Fields

        private int _currentInstrument;

        private Dropdown _dropdown;

#pragma warning disable CS0649
        [SerializeField] private Instrument[] _instruments;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Private Methods

        // Start is called before the first frame update
        private void Start()
        {
            _dropdown = GetComponent<Dropdown>();

            foreach (var insrt in _instruments)
            {
                insrt.ui.SetActive(false);
            }

            var titles = (from x in _instruments
                          select x.title).ToList();
            _dropdown.AddOptions(titles);

            _dropdown.OnValueChangedAsObservable()
                     .Subscribe(x =>
                     {
                         _instruments[_currentInstrument].ui.SetActive(false);
                         _currentInstrument = x;
                         _instruments[x].ui.SetActive(true);
                     });
            _dropdown.value = 0;
        }

        #endregion Private Methods
    }
}
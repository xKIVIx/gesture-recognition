﻿using Oranges.Data;
using System;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Oranges.UI.Components
{
    internal class CameraSatus : MonoBehaviour
    {
#pragma warning disable CS0649

        #region Private Fields

        [SerializeField] private Button _buttonStart;
        [SerializeField] private StatusSwaper _buttonStatus;
        [SerializeField] private StatusSwaper _connectedSatus;
        private DeviceInfo _deviceInfo;
        private List<IDisposable> _disposables = new List<IDisposable>();
        private IObservable<Unit> _observableButton;
        [SerializeField] private StatusSwaper _startStatus;
        [SerializeField] private TextMeshProUGUI _title;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        public DeviceInfo DeviceInfo
        {
            get => _deviceInfo;
            set
            {
                _deviceInfo = value;
                _buttonStatus.IsTrue = !_deviceInfo.isStarted;
                _connectedSatus.IsTrue = _deviceInfo.isConnected;
                _startStatus.IsTrue = _deviceInfo.isStarted;
                _title.text = _deviceInfo.title;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public IObservable<DeviceInfo> GetObservableStartButton() => _buttonStart.OnClickAsObservable()
                                                                                 .Select(x => { return _deviceInfo; });

        #endregion Public Methods
    }
}
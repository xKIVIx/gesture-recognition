﻿using Oranges.Data;

using UnityEngine;

namespace Oranges.UI.Components
{
    internal class RenderHand : MonoBehaviour
    {
#pragma warning disable CS0649

        #region Private Fields

        /// <summary>
        /// Префаб пальца
        /// </summary>
        [SerializeField]
        private LineRenderer _prefabFinger;

        /// <summary>
        /// Префаб ладони
        /// </summary>
        [SerializeField]
        private LineRenderer _prefabPalm;

        private LineRenderer[] _renders;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Methods

        /// <summary>
        /// Отрисовка скелета руки.
        /// </summary>
        /// <param name="handData">
        /// Данные костей руки.
        /// </param>
        /// <param name="vectorCentered">
        /// Вектор цетрализации отображения.
        /// </param>
        public void RendHand(HandData handData, Vector3 vectorCentered)
        {
            var countRenders = handData.Fingers.Length + 1;

            if (_renders?.Length != countRenders && _renders != null)
            {
                foreach (var render in _renders)
                {
                    DestroyImmediate(render.gameObject);
                }
                _renders = null;
            }

            if (_renders == null)
            {
                _renders = new LineRenderer[countRenders];
            }

            var renderId = 0;

            for (var i = 0; i < handData.Fingers.Length; i++)
            {
                if (_renders[renderId] == null)
                {
                    _renders[renderId] = Instantiate(_prefabFinger, transform);
                }

                var fingerPoints = handData.Fingers[i].Clone() as Vector3[];
                for (var k = 0; k < fingerPoints.Length; k++)
                {
                    fingerPoints[k] += vectorCentered;
                }
                _renders[renderId].positionCount = fingerPoints.Length;
                _renders[renderId].SetPositions(fingerPoints);
                renderId++;
            }

            var palmPoints = handData.Palm.Clone() as Vector3[];
            for (var k = 0; k < palmPoints.Length; k++)
            {
                palmPoints[k] += vectorCentered;
            }
            if (_renders[renderId] == null)
            {
                _renders[renderId] = Instantiate(_prefabPalm, transform);
            }
            _renders[renderId].positionCount = palmPoints.Length;
            _renders[renderId].SetPositions(palmPoints);
        }

        #endregion Public Methods
    }
}
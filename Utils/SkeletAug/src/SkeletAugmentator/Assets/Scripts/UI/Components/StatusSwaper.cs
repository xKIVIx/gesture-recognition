﻿using UnityEngine;

namespace Oranges.UI.Components
{
    internal class StatusSwaper : MonoBehaviour
    {
#pragma warning disable CS0649

        #region Private Fields

        [SerializeField] private bool _isTrue;

        /// <summary>
        /// Объект отображаемые в состоянии ложь.
        /// </summary>
        [SerializeField] private GameObject _viewFalse;

        /// <summary>
        /// Объект отображаемые в состоянии истина.
        /// </summary>
        [SerializeField] private GameObject _viewTrue;

        #endregion Private Fields

#pragma warning restore CS0649

        #region Public Properties

        /// <summary>
        /// Состояние переключателя.
        /// </summary>
        public bool IsTrue
        {
            get => _viewTrue.activeSelf;

            set
            {
                _viewTrue.SetActive(value);
                _viewFalse.SetActive(!value);
            }
        }

        #endregion Public Properties

        #region Private Methods

        private void OnValidate() => IsTrue = _isTrue;

        #endregion Private Methods
    }
}
﻿using Oranges.Data;
using Oranges.Data.Server;
using System;
using UniRx;

namespace Oranges.UI
{
    /// <summary>
    /// Менеджер управляющий UI.
    /// </summary>
    public interface IUIManager
    {
        #region Public Properties

        /// <summary>
        /// Параметры аугментатора.
        /// </summary>
        ReactiveProperty<AugmentatorSettings> AugmentatorSettings { get; }

        /// <summary>
        /// Количество уже записаных примеров.
        /// </summary>
        IntReactiveProperty CountRecordedExamples { get; }

        /// <summary>
        /// Частота записи с камер.
        /// </summary>
        IntReactiveProperty CurrentFrameRateRecord { get; }

        BoolReactiveProperty DemoServerIsConnected { get; }

        StringReactiveProperty DemoServerResult { get; }

        /// <summary>
        /// Частота записи с камер.
        /// </summary>
        IntReactiveProperty FrameRateRecord { get; }

        /// <summary>
        /// Аугментированные данные для рендера руки.
        /// </summary>
        ReactiveProperty<HandData[]> GestureAugment { get; }

        /// <summary>
        /// Данные для рендера руки.
        /// </summary>
        ReactiveProperty<HandData[]> GestureOriginal { get; }

        /// <summary>
        /// Состояние готовности к началу записи.
        /// </summary>
        bool IsRecordReady { get; set; }

        /// <summary>
        /// Показать экран загрузки.
        /// </summary>
        bool IsViewLoadscreen { get; set; }

        /// <summary>
        /// Получение IObservable для задания логики при
        /// вызов аугментации данных из списка файлов.
        /// </summary>
        /// <returns></returns>
        IObservable<string[]> ObservableAugmentFiles { get; }

        /// <summary>
        /// Нажатие на кноку запуска устройства.
        /// </summary>
        /// <returns></returns>
        IObservable<int> ObservableStartDevice { get; }

        /// <summary>
        /// Запуск записи.
        /// </summary>
        IObservable<Unit> ObservableStartRecord { get; }

        /// <summary>
        /// Путь открытого файла.
        /// </summary>
        StringReactiveProperty PathOpenedFile { get; }

        /// <summary>
        /// Процент выполнения задачи. Выводится на load screen.
        /// </summary>
        float ProcentLoad { get; set; }

        /// <summary>
        /// Список устройств для записи.
        /// </summary>
        ReactiveCollection<DeviceInfo> RecordDiveces { get; }

        /// <summary>
        /// Путь для сохранения аугментированых файлов.
        /// </summary>
        string SavePathAugFiles { get; }

        /// <summary>
        /// Петь сохранения записаных данных.
        /// </summary>
        StringReactiveProperty SavePathRecordFiles { get; }

        IObservable<Unit> Start { get; }

        /// <summary>
        /// Название записываемого жеста.
        /// </summary>
        StringReactiveProperty TitleRecordGesture { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Подключение к демосерверу
        /// </summary>
        IObservable<ConnectData> ObservableDemoServerConnect();

        /// <summary>
        /// Отключение от демо сервера
        /// </summary>
        IObservable<Unit> ObservableDemoServerDisconnect();

        #endregion Public Methods
    }
}
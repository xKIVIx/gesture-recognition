# TCN
| Кол-во жестов | Обучающая выборка | Тестовая выборка | Количество эпох | Размер эпохи | Random seed | Dataset |
| ------------- | ----------------- | ---------------- | --------------- | ------------ | ----------- | ------- |
| 14 | 23706 | 1649 | 3 | 128 | 87118956 | [Dynamic Hand Gesture 14/28 dataset](http://www-rech.telecom-lille.fr/DHGdataset/) + [SHREC’17 Track Dataset](http://www-rech.telecom-lille.fr/shrec2017-hand/) координаты костей в пространстве + агуметация |

![](classesCount.png)
## Результаты
### Batchs loss
![](batchs_loss.png)
### Acc
![](acc.png)
### Loss
![](loss.png)

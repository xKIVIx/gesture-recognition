# experement_11
| Кол-во жестов | Обучающая выборка | Тестовая выборка | Количество эпох | Размер эпохи | Random seed | Dataset |
| ------------- | ----------------- | ---------------- | --------------- | ------------ | ----------- | ------- |
| 14 | 15744 | 1664 | 200 | 256 | 52921555 | [Dynamic Hand Gesture 14/28 dataset](http://www-rech.telecom-lille.fr/DHGdataset/) + [SHREC’17 Track Dataset](http://www-rech.telecom-lille.fr/shrec2017-hand/) координаты костей в пространстве + агуметация |

![](classesCount.png)
## Результаты
### Точность
![](acc.png)
### Функция потерь
![](loss.png)

from enum import IntEnum

class MessageType(IntEnum):
    GET_RESULT           = 0,
    DATA_BONES           = 1,
    NEW_SESSION          = 2,
    ERROR_COMPUTE_RESULT = 3,
    SEND_RESULT          = 4
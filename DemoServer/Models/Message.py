from Models.MessageType import MessageType

class Message():
    def __init__(self, message_type:MessageType):
        self.type = message_type
        self.data = None
from Controllers.ServerController import ServerController
from Controllers.RecognizeLSTMController import RecognizeLSTMController
from Models.MessageType import MessageType

recognizer = RecognizeLSTMController()
server = ServerController()
server.WaitClient()

while True:
    request = server.GetRequest()
    
    if request.type == MessageType.NEW_SESSION:
        recognizer.ClearState()
    elif request.type == MessageType.DATA_BONES:
        recognizer.SendData(request.data)
    elif request.type == MessageType.GET_RESULT:
        server.SendResult( recognizer.GetResult() )
    else:
        print(f"Unknown message type: {request.type}")
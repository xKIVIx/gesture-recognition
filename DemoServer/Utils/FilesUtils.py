import json
from pathlib import Path

def GetSettings(settings_name:str):
    root_dir = GetRootPath()

    with open(f"{root_dir}/settings.json", "r") as read_file:
        return json.load(read_file)[settings_name]

def GetRootPath():
    return f"{Path(__file__).parent}/.."
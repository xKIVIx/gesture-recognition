import json
import FilesUtils
from comet_ml import APIExperiment
import os

def GetParametrs(api:APIExperiment):
    parametrs = api.get_parameters_summary()
    result = {}
    for p in parametrs:
        result[p["name"]] = p["valueCurrent"]
    return result

def GetModels(api:APIExperiment):
    step = 0
    models = {}
    assets = api.get_asset_list()
    for a in assets:
        if ".pt" in a["fileName"]:
            if step <= a["step"]:
                step = a["step"]
                models[a["fileName"]] = a["assetId"]
    
    result = {}
    for k, i in models.items():
        result[k] = api.get_asset(i, return_type="binary")
    return result

if __name__ == "__main__":
    root_dir = FilesUtils.GetRootPath()

    settings = FilesUtils.GetSettings("cometml")
    api_key = settings["api_key"]
    dowload_experements = settings["dowload_experements"]

    nn_models_dir = f"{root_dir}/NN_models"

    if not os.path.isdir(nn_models_dir):
        os.mkdir(nn_models_dir)

    for exp_id in dowload_experements:
        api_experiment = APIExperiment(previous_experiment=exp_id, api_key=api_key)
        name = api_experiment.get_name()
        parametrs = GetParametrs(api_experiment)
        models = GetModels(api_experiment)

        model_dir = f"{nn_models_dir}/{name}"
        if not os.path.isdir(model_dir):
            os.mkdir(model_dir)

        with open(f"{model_dir}/parametrs.json", "w") as file:
            json.dump(parametrs, file)
        
        for name, model in models.items():
            with open(f"{model_dir}/{name}", "wb") as file:
                file.write(model)


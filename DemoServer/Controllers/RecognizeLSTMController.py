from torch import nn
import torch
import json
import Utils.FilesUtils as fu

class RecognizeLSTMController(nn.Module):
    def __init__(self):
        super(RecognizeLSTMController, self).__init__()
        model_path = f"{fu.GetRootPath()}/NN_models/Seq2One"
        with open(f"{model_path}/parametrs.json") as file:
            parametrs = json.load(file)
        self._input_size = int(parametrs["input_size"])
        self._output_size = int(parametrs["output_size"])
        self._n_layers = int(parametrs["n_layers"])
        self._dropout = float(parametrs["dropout"])
        self._hidden_size = int(parametrs["hide_dim"])
        self._vocab = parametrs["vocab"]

        self._gru = nn.GRU(self._input_size, 
                           self._hidden_size, 
                           self._n_layers, 
                           dropout = self._dropout, 
                           batch_first = True)
        self._liner = nn.Linear(self._hidden_size, self._output_size)

        self._device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.to(self._device)

        self.load_state_dict(torch.load(f"{model_path}/model.pt"))
        self.eval()

        self.ClearState()
    
    def SendData(self, input):
        with torch.no_grad():
            input = torch.Tensor(input).to(self._device)
            input = input.view(1,1,-1)
            out, self._hidden = self._gru(input, self._hidden)
            out = self._liner(out)
            self._result = nn.functional.log_softmax(out, dim=2).squeeze()

    def GetResult(self):
        if self._result == None:
            return ""
        else :
            return self._vocab[torch.argmax(self._result).item()]

    def ClearState(self):
        self._hidden = None
        self._result = None
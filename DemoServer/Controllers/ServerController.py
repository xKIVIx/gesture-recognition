import Utils.FilesUtils as fu
import socket
import struct
from Models.MessageType import MessageType
from Models.Message import Message

class ServerController:
    def __init__(self):
        settings = fu.GetSettings("server")

        self._host = settings["host"]
        self._port = int(settings["port"])

        self._socket = socket.socket()
        self._socket.bind((self._host, self._port))

        self._client = None

    def WaitClient(self):
        print(f"Server: start listen")
        self._socket.listen(1)
        print(f"Server: wait client...")
        self._client, _ = self._socket.accept()
        print(f"Server: client connected")

    def GetRequest(self):
        while True:
            data = self._client.recv(4)
            if data != b"":
                print(f"Server: come message type:{data}")
                message_type = int.from_bytes(data, byteorder='little')
                message_type = MessageType( message_type )
                break

        result = Message( message_type )

        if message_type == MessageType.DATA_BONES:
            size = self._client.recv(4)
            size = int.from_bytes(size, byteorder='little')
            data = self._client.recv(size)
            data = list( struct.iter_unpack('f', data) )
            result.data = data
        return result


    def SendResult(self, result):
        print(f"Send result: {result}")
        message = MessageType.SEND_RESULT.value.to_bytes(4, byteorder='little')
        data = f"{result}".encode('utf8')
        message += len(data).to_bytes(4, byteorder='little')
        message += data
        self._client.send(message)


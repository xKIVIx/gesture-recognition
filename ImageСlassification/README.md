# Распознавание жестов изображений на основе картинок
## Датасет
[link](https://github.com/ardamavi/Sign-Language-Digits-Dataset)
* 10 классов
* 205 картинок на каждый класс
* Обучающая выборка - 80 %
* Тестовая - 10 %
* Валидация - 10 %

## Результаты
### Lenet+keras
После аугментации обучающая выборка стала 34560 изображений всего

**Гиперпараметры**
* Epoch - 15
* Learning rate - 0.001
* Batch size - 800
* Оптимизатор - ADAM
* Loss - categorical_crossentropy

**Вывод**
* `Test - 96.5 %`
* `Train Loss: 0.4831 Acc: 0.8362`
* `Val Loss: 0.1117 Acc: 0.9656`
* `Training complete in 2m 10s`


### VGG16+pytorch
Применялась аугментация на лету  
Изменял веса только на последних трех слоях:  
`nn.Linear(4096,1000)`  
`nn.Linear(1000,100)`  
`nn.Linear(100,10)`


**Гиперпараметры**
* Epoch - 20
* Learning rate - 0.001
* Batch size - 4
* Оптимизатор - SGD
* Loss - nn.CrossEntropyLoss()

**Вывод**
* `Test - 86.1 %`
* `Train Loss: 1.6948 Acc: 0.5268`
* `Val Loss: 0.7049 Acc: 0.7571`
* `Training complete in 6m 42s`


### VGG19+pytorch
Применялась аугментация на лету  
Изменял веса только на последнем слое : `nn.Linear(4096,10)`

**Гиперпараметры**
* Epoch - 20
* Learning rate - 0.001
* Batch size - 4
* Оптимизатор - SGD
* Loss - nn.CrossEntropyLoss()

**Вывод**
* `Test - 81.1 %`
* `Train Loss: 1.3523 Acc: 0.5408`
* `Val Loss: 1.0691 Acc: 0.7381`
* `Training complete in ?m ??s`
